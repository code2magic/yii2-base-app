<?php
/**
 * @var array $params
 *
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
return [
    'id' => 'frontend',
    'language' => 'uk',
    'layout' => 'main',
    'container' => [
        'definitions' => [
            \yii\twig\ViewRenderer::class => [
                // Array of twig options:
                'options' => YII_DEBUG ? [
                    'debug' => true,
                    'auto_reload' => true,
                ] : [],
                'extensions' => YII_DEBUG ? [
                    \Twig\Extension\DebugExtension::class,
                ] : [],
                'functions' => [
                    'call' => function ($className, $method, $arguments = null) {
                        $callable = [str_replace('/', '\\', $className), $method];
                        if ($arguments === null) {
                            return call_user_func($callable);
                        }
                        return call_user_func_array($callable, $arguments);
                    },
                ],
            ],
            \yii\web\View::class => [
                'defaultExtension' => 'twig',
                'renderers' => [
                    'twig' => \yii\twig\ViewRenderer::class,
                ],
            ],
        ],
        'singletons' => [
            \yii\web\Request::class => [
                'cookieValidationKey' => $params['request_cookie_validation.frontend'],
            ],
        ],
    ],
    'components' => [
        'urlManager' => 'urlManager-frontend',
    ],
];

<?php
/**
 * @var array $params
 *
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
return [
    'id' => 'backend',
    'defaultRoute' => '/timeline-event/index',
    'container' => [
        'definitions' => [
            \alexantr\ckeditor\CKEditor::class => [
                'presetPath' => '@backend/config/_ckeditor.php',
            ],
            \alexantr\elfinder\InputFile::class => [
                'clientRoute' => '//file/manager/input',
                'options' => [
                    'class' => 'form-control yii2-elfinder-input input_preview',
                    'data' => [
                        'default-src' => '/no_image.png',
                        'storage-src' => Yii::getAlias('@storageUrl/source'),
                    ],
                ],
                'filter' => ['image',],
            ],
            \trntv\filekit\widget\Upload::class => [
                'url' => ['/file/storage/upload',],
                'maxFileSize' => 10485760, // 10 MiB
            ],
            \kartik\tabs\TabsX::class => [
                'containerOptions' => [
                    'class' => 'nav-tabs-custom',
                ],
                'encodeLabels' => false,
                'enableStickyTabs' => true,
                'stickyTabsOptions' => [
                    'selectorAttribute' => 'data-target',
                    'backToTop' => false,
                ],
            ],
            \unclead\multipleinput\MultipleInput::class => [
                'addButtonPosition' => \unclead\multipleinput\MultipleInput::POS_FOOTER,
                'enableError' => true,
            ],
            \yii\web\AssetManager::class => [
                'bundles' => [
                    \alexantr\elfinder\ElFinderAsset::class => [
                        'js' => [
                            'js/elfinder.min.js',
                            'js/extras/editors.default.min.js',
                            'js/extras/quicklook.googledocs.js',
                        ],
                    ],
                ],
            ],
        ],
        'singletons' => [
            \code2magic\core\web\helpers\JsVarsHelper::class => [
                'js_vars' => [
                    'app_config' => function () {
                        return [
                            'urls' => [
                                'api' => \Yii::getAlias('@apiUrl'),
                                'frontend' => \Yii::getAlias('@frontendUrl'),
                                'backend' => \Yii::getAlias('@backendUrl'),
                                'storage' => \Yii::getAlias('@storageUrl'),
                            ],
                        ];
                    },
                ],
            ],
            \yii\web\Request::class => [
                'cookieValidationKey' => $params['request_cookie_validation.backend'],
            ],
        ],
    ],
    'components' => [
        'urlManager' => 'urlManager-backend',
        'cache_api' => 'cache-api',
    ],
    'controllerMap' => [
        'callback' => \code2magic\baseApp\backend\controllers\CallbackController::class,
        'sign-in' => \code2magic\baseApp\backend\controllers\SignInController::class,
        'site' => \code2magic\baseApp\backend\controllers\SiteController::class,
        'timeline-event' => \code2magic\baseApp\backend\controllers\TimelineEventController::class,
    ],
    'modules' => [
        'file' => [
            'class' => \yii\base\Module::class,
            'controllerNamespace' => 'code2magic\baseApp\backend\modules\file\controllers',
        ],
        'system' => [
            'class' => \yii\base\Module::class,
            'controllerNamespace' => 'code2magic\baseApp\backend\modules\system\controllers',
        ],
    ],
    'as locale' => [
        'class' => \code2magic\i18n\behaviors\LocaleBehavior::class,
        'enablePreferredLanguage' => true,
    ],
];

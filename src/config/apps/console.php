<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
return [
    'id' => 'console',
    'basePath' => dirname(__DIR__),
    'controllerMap' => [
        'migrate' => [
            'class' => \yii\console\controllers\MigrateController::class,
            'migrationTable' => '{{%system_db_migration}}',
            'migrationPath' => [
                '@yii/rbac/migrations',
            ],
            'migrationNamespaces' => [
                'code2magic\baseApp\migrations',
                'code2magic\baseApp\components\keyStorage\migrations',
            ],
        ],
    ],
];

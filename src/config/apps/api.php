<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
return [
    'id' => 'api',
    'basePath' => dirname(__DIR__),
    'homeUrl' => \Yii::getAlias('@apiUrl'),
    'container' => [
        'definitions' => [
            \yii\web\Request::class => [
                'enableCookieValidation' => false,
            ],
        ],
        'singletons' => [
        ],
    ],
    'components' => [
        'urlManager' => 'urlManager-api',
        'cache' => 'cache-api',
    ],
    //'controllerNamespace' => 'code2magic\baseApp\api\controllers',
    'controllerMap' => [
        'site' => \code2magic\baseApp\api\controllers\SiteController::class,
    ],
    'modules' => [
    ],
];

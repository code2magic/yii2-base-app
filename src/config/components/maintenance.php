<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
if (YII_DEBUG) {
    return [];
}
return [
    'bootstrap' => ['maintenance',],
    'container' => [
        'definitions' => [
        ],
        'singletons' => [
            \code2magic\baseApp\components\maintenance\Maintenance::class => [
                'enabled' => function ($app) {
                    return $app->params['app.maintenance'] || $app->keyStorage->get('frontend.maintenance') === 'enabled';
                },
            ],
        ],
    ],
    'components' => [
        'maintenance' => \code2magic\baseApp\components\maintenance\Maintenance::class,
    ],
];

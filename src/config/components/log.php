<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
$targets = [
    'main' => [
        '__class' => \yii\log\DbTarget::class,
        'levels' => ['error', 'warning',],
        'except' => ['yii\web\HttpException:*', 'yii\i18n\I18N\*',],
        'prefix' => function () {
            $url = !Yii::$app->request->isConsoleRequest ? Yii::$app->request->getUrl() : null;
            return sprintf('[%s][%s]', Yii::$app->id, $url);
        },
        'logVars' => [],
        'logTable' => '{{%system_log}}',
    ],
];
if (YII_ENV === 'prod' && !(empty($params['email.robot']) || empty($params['email.webmaster']))) {
    $targets['email'] = [
        '__class' => yii\log\EmailTarget::class,
        'except' => ['yii\web\HttpException:*', 'yii\i18n\I18N\*',],
        'levels' => ['error',/*/ 'warning',/**/],
        'message' => ['from' => $params['email.robot'], 'to' => $params['email.webmaster'],],
    ];
}
return [
    'container' => [
        'definitions' => [
            \yii\log\Dispatcher::class => [
                'traceLevel' => YII_DEBUG ? 3 : 0,
                'targets' => $targets,
            ],
        ],
        'singletons' => [
        ],
    ],
];

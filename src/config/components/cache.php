<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
return [
    'container' => [
        'definitions' => [
        ],
        'singletons' => [
            'cache-main' => YII_ENV === 'dev'
                ? \yii\caching\DummyCache::class
                : [
                    '__class' => \yii\caching\FileCache::class,
                    'cachePath' => '@common/runtime/cache',
                ],
            'cache-api' => YII_ENV === 'dev'
                ? \yii\caching\DummyCache::class
                : [
                    '__class' => yii\caching\FileCache::class,
                    'cachePath' => '@common/runtime/cache-api',
                ],
            'cache-outer-api' => [
                '__class' => \yii\caching\FileCache::class,
                'cachePath' => '@common/runtime/cache-outer-api',
                'defaultDuration' => 86400,
            ],
        ],
    ],
    'components' => [
        'cache' => 'cache-main',
        'cache_outer_api' => 'cache-outer-api',
    ],
];

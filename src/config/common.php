<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
return [
    'sourceLanguage' => 'en-US',
    'bootstrap' => ['log',],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
    ],
];

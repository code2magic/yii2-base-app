<?php

namespace code2magic\baseApp\frontend\widgets;

use code2magic\i18n\helpers\Locale;
use code2magic\i18n\models\Language;
use Yii;
use yii\base\Widget;
use yii\behaviors\CacheableWidgetBehavior;
use yii\caching\TagDependency;

/**
 * Class LanguageList
 * @package code2magic\baseApp\frontend\widgets
 */
class LanguageList extends Widget
{
    /**
     * @var string
     */
    public $view_wrapper = 'language.twig';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => CacheableWidgetBehavior::class,
                'cacheDuration' => 3600,
                'cacheKeyVariations' => [
                    Yii::$app->language,
                    $this->view_wrapper,
                    Yii::$app->controller->route,
                    Yii::$app->request->queryParams,
                ],
                'cacheDependency' => [
                    'class' => TagDependency::class,
                    'tags' => [Language::class,],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render($this->view_wrapper, ['language_items' => Locale::getLanguageLinksData(),]);
    }
}

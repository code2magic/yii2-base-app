<?php

namespace code2magic\baseApp\frontend\widgets;

use code2magic\baseApp\components\keyStorage\models\KeyStorageItem;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\behaviors\CacheableWidgetBehavior;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;

/**
 * Class PhoneWidget
 * @package code2magic\baseApp\frontend\widgets
 */
class PhoneWidget extends Widget
{
    /**
     * @var string
     */
    public $view_path = 'phones.twig';

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        if (!$this->view_path) {
            throw new InvalidConfigException('View path must be set');
        }
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => CacheableWidgetBehavior::class,
                'cacheDuration' => 3600,
                'cacheKeyVariations' => [
                    Yii::$app->language,
                    $this->view_path,
                ],
                'cacheDependency' => [
                    'class' => TagDependency::class,
                    'tags' => KeyStorageItem::class . 'frontend.contact_phones',
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function run()
    {
        $phones = Yii::$app->keyStorage->get('frontend.contact_phones', []);
        $phone_items = [];
        foreach ($phones as $phone) {
            $item = ArrayHelper::getValue($phone, 'label');
            $phone_items[] = [
                'label' => $item,
                //'link' => Yii::$app->formatter->asPhone(ArrayHelper::getValue($phone, 'phone'), Yii::$app->language),
                'phone' => Yii::$app->formatter->format(ArrayHelper::getValue($phone, 'phone'), 'phone'),
            ];
        }
        return $this->render($this->view_path, ['models' => $phone_items,]);
    }
}

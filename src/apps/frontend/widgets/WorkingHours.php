<?php

namespace code2magic\baseApp\frontend\widgets;

use code2magic\baseApp\components\keyStorage\models\KeyStorageItem;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\behaviors\CacheableWidgetBehavior;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;

/**
 * Class WorkingHours
 * @package code2magic\baseApp\frontend\widgets
 */
class WorkingHours extends Widget
{
    /**
     * @var string
     */
    public $view_path = 'working-hours.twig';

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        if (!$this->view_path) {
            throw new InvalidConfigException('View path must be set');
        }
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => CacheableWidgetBehavior::class,
                'cacheDuration' => 3600,
                'cacheKeyVariations' => [
                    Yii::$app->language,
                    $this->view_path,
                ],
                'cacheDependency' => [
                    'class' => TagDependency::class,
                    'tags' => KeyStorageItem::class . 'frontend.working_hours',
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render($this->view_path, [
            'models' => ArrayHelper::getColumn(
                Yii::$app->keyStorage->get('frontend.working_hours', []),
                'line',
                false
            ),
        ]);
    }
}

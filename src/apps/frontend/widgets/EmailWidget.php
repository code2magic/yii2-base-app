<?php

namespace code2magic\baseApp\frontend\widgets;

use code2magic\baseApp\components\keyStorage\models\KeyStorageItem;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\behaviors\CacheableWidgetBehavior;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;

/**
 * Class EmailWidget
 * @package code2magic\baseApp\frontend\widgets
 */
class EmailWidget extends Widget
{
    /**
     * @var string
     */
    public $view_path = 'emails.twig';

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        if (!$this->view_path) {
            throw new InvalidConfigException('View path must be set');
        }
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => CacheableWidgetBehavior::class,
                'cacheDuration' => 3600,
                'cacheKeyVariations' => [
                    Yii::$app->language,
                    $this->view_path,
                ],
                'cacheDependency' => [
                    'class' => TagDependency::class,
                    'tags' => KeyStorageItem::class . 'common.admin_emails',
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function run()
    {
        $emails = Yii::$app->keyStorage->get('common.admin_emails', []);
        $email_items = [];
        foreach ($emails as $email) {
            $item = ArrayHelper::getValue($email, 'name');
            $email_items[] = [
                'name' => $item,
                'email' => ArrayHelper::getValue($email, 'email'),
            ];
        }
        return $this->render($this->view_path, ['models' => $email_items,]);
    }
}

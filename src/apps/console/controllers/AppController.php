<?php

namespace code2magic\baseApp\console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class AppController extends Controller
{
    /** @var array */
    public $writablePaths = [
        '@api/runtime',
        '@backend/runtime',
        '@backend/web/assets',
        '@base/dbbackups',
        '@common/runtime',
        '@frontend/runtime',
        '@frontend/web/assets',
        '@storage/web/cache',
        '@storage/web/source',
        '@vendor/hiqdev/composer-config-plugin-output',
    ];

    /** @var array */
    public $executablePaths = [
        '@backend/yii',
        '@frontend/yii',
        '@console/yii',
        '@api/yii',
        '@base/yii',
        '@base/test/bin/yii',
    ];

    /** @var array */
    public $generateKeysPaths = [
        '@base/.env',
    ];

    /** @var array */
    public $clearPaths = [
        '@api/runtime',
        '@backend/runtime',
        '@backend/web/assets',
        '@common/runtime',
        '@console/runtime',
        '@frontend/runtime',
        '@frontend/web/assets',
    ];

    /**
     * Sets given keys to .env file
     */
    public function actionSetKeys()
    {
        $this->setKeys($this->generateKeysPaths);
    }

    /**
     * @param $paths
     */
    private function setKeys($paths)
    {
        Console::output(Console::ansiFormat('======= Generating keys =======', [Console::FG_GREEN,]));
        foreach ($paths as $file) {
            $file = Yii::getAlias($file);
            Console::output(Console::ansiFormat("== Generating keys in {$file} ==", [Console::FG_CYAN,]));
            $content = file_get_contents($file);
            $content = preg_replace_callback('/<generated_key>/', function () {
                $length = 32;
                $bytes = openssl_random_pseudo_bytes(32, $isSourceStrong);
                if (false === $isSourceStrong || false === $bytes) {
                    throw new \RuntimeException('IV generation failed');
                }
                return strtr(substr(base64_encode($bytes), 0, $length), '+/', '_-');
            }, $content);
            file_put_contents($file, $content);
        }
    }

    /**
     * @throws \yii\base\InvalidRouteException
     * @throws \yii\console\Exception
     */
    public function actionSetup()
    {
        $this->runAction('set-writable', ['interactive' => $this->interactive,]);
        $this->runAction('set-executable', ['interactive' => $this->interactive,]);
        $this->runAction('set-keys', ['interactive' => $this->interactive,]);
        $this->runAction('clear-paths', ['interactive' => $this->interactive,]);
        Console::output(Console::ansiFormat('======= Apply migrations =======', [Console::FG_GREEN,]));
        Console::output(Console::ansiFormat("== Apply migrate ==", [Console::FG_CYAN,]));
        \Yii::$app->runAction('migrate/up', ['interactive' => $this->interactive,]);
        Console::output(Console::ansiFormat("== Apply rbac-migrate ==", [Console::FG_CYAN,]));
        \Yii::$app->runAction('rbac-migrate/up', ['interactive' => $this->interactive,]);
    }

    /**
     * Clear runtime data
     */
    public function actionClearPaths()
    {
        Console::output(Console::ansiFormat('======= Clearing runtime data =======', [Console::FG_GREEN,]));
        foreach ($this->clearPaths as $clearPath) {
            if ($dir = Yii::getAlias($clearPath, false)) {
                foreach (\yii\helpers\FileHelper::findDirectories($dir, ['recursive' => false,]) as $dir) {
                    Console::output(Console::ansiFormat("== Clearing runtime: {$dir} ==", [Console::FG_CYAN,]));
                    \yii\helpers\FileHelper::removeDirectory($dir);
                }
            }
        }
    }

    /**
     * Truncates all tables in the database.
     * @throws \yii\db\Exception
     */
    public function actionTruncate()
    {
        $dbName = Yii::$app->db->createCommand('SELECT DATABASE()')->queryScalar();
        if ($this->confirm('This will truncate all tables of current database [' . $dbName . '].')) {
            Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS=0')->execute();
            $tables = Yii::$app->db->schema->getTableNames();
            foreach ($tables as $table) {
                $this->stdout('Truncating table ' . $table . PHP_EOL, Console::FG_RED);
                Yii::$app->db->createCommand()->truncateTable($table)->execute();
            }
            Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS=1')->execute();
        }
    }

    /**
     * Drops all tables in the database.
     * @throws \yii\db\Exception
     */
    public function actionDrop()
    {
        $dbName = Yii::$app->db->createCommand('SELECT DATABASE()')->queryScalar();
        if ($this->confirm('This will drop all tables of current database [' . $dbName . '].')) {
            Yii::$app->db->createCommand("SET foreign_key_checks = 0")->execute();
            $tables = Yii::$app->db->schema->getTableNames();
            foreach ($tables as $table) {
                $this->stdout('Dropping table ' . $table . PHP_EOL, Console::FG_RED);
                Yii::$app->db->createCommand()->dropTable($table)->execute();
            }
            Yii::$app->db->createCommand("SET foreign_key_checks = 1")->execute();
        }
    }

    /**
     * @param string $charset
     * @param string $collation
     * @throws \yii\base\ExitException
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\Exception
     */
    public function actionAlterCharset($charset = 'utf8mb4', $collation = 'utf8mb4_unicode_ci')
    {
        if (Yii::$app->db->getDriverName() !== 'mysql') {
            Console::error('Only mysql is supported');
            Yii::$app->end(1);
        }

        if (!$this->confirm("Convert tables to character set {$charset}?")) {
            Yii::$app->end();
        }

        $tables = Yii::$app->db->getSchema()->getTableNames();
        Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS = 0')->execute();
        foreach ($tables as $table) {
            $command = Yii::$app->db->createCommand("ALTER TABLE {$table} CONVERT TO CHARACTER SET :charset COLLATE :collation")->bindValues([
                ':charset' => $charset,
                ':collation' => $collation,
            ]);
            $command->execute();
        }
        Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS = 1')->execute();
        Console::output('All ok!');
    }

    /**
     * Adds write permissions
     */
    public function actionSetWritable()
    {
        $this->setWritable($this->writablePaths);
    }

    /**
     * @param $paths
     */
    private function setWritable($paths)
    {
        $filemode = 0777;
        Console::output(Console::ansiFormat('======= Setting writable paths =======', [Console::FG_GREEN,]));
        foreach ($paths as $writable) {
            $writable = Yii::getAlias($writable);
            Console::output(Console::ansiFormat("== Setting writable: {$writable} ==", [Console::FG_CYAN,]));
            //@$this->chmod_R($writable, $filemode);
            $iterator = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($writable),
                \RecursiveIteratorIterator::SELF_FIRST
            );
            foreach ($iterator as $item) {
                if (is_link($item) || in_array(basename($item), ['.', '..',])) {
                    Console::output(Console::ansiFormat("link '{$item}' is skipped", [Console::FG_YELLOW,]));
                    continue;
                }
                if (!@chmod($item, $filemode)) {
                    $dirmode_str = decoct($filemode);
                    Console::output(Console::ansiFormat("Failed applying filemode '{$dirmode_str}' on directory '{$item}'", [Console::FG_RED,]));
                    Console::output(Console::ansiFormat("  `-> the directory '{$item}' will be skipped from recursive chmod", [Console::FG_YELLOW,]));
                }
            }
        }
    }

    private function chmod_R($path, $filemode, $dirmode = null)
    {
        $dirmode = $dirmode ?? $filemode;
        if (is_dir($path)) {
            if (!chmod($path, $dirmode)) {
                $dirmode_str = decoct($dirmode);
                Console::output(Console::ansiFormat("Failed applying filemode '$dirmode_str' on directory '$path'", [Console::FG_RED,]));
                return false;
            }
            $dh = opendir($path);
            while (($file = readdir($dh)) !== false) {
                if ($file !== '.' && $file !== '..') {  // skip self and parent pointing directories
                    $fullpath = $path . '/' . $file;
                    if (!$this->chmod_R($fullpath, $filemode, $dirmode)) {
                        return false;
                    }
                }
            }
            closedir($dh);
        } else {
            if (is_link($path)) {
                Console::output(Console::ansiFormat("link '$path' is skipped", [Console::FG_YELLOW,]));
                return true;
            }
            if (!chmod($path, $filemode)) {
                $filemode_str = decoct($filemode);
                Console::output(Console::ansiFormat("Failed applying filemode '$filemode_str' on file '$path'", [Console::FG_RED,]));
                return false;
            }
        }
        return true;
    }

    /**
     * Adds execute permissions
     */
    public function actionSetExecutable()
    {
        $this->setExecutable($this->executablePaths);
    }

    /**
     * @param $paths
     */
    private function setExecutable($paths)
    {
        Console::output(Console::ansiFormat('======= Setting executable paths =======', [Console::FG_GREEN,]));
        foreach ($paths as $executable) {
            $executable = Yii::getAlias($executable);
            Console::output(Console::ansiFormat("== Setting executable: {$executable} ==", [Console::FG_CYAN,]));
            @chmod($executable, 0755);
        }
    }
}

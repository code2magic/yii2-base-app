<?php

namespace code2magic\baseApp\api\controllers;

use Yii;
use yii\rest\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * Class SiteController
 * @package code2magic\baseApp\api\controllers
 */
class SiteController extends Controller
{
    /**
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->redirect(\Yii::getAlias('@frontendUrl'));
    }

    /**
     * @return mixed
     */
    public function actionError()
    {
        if (($exception = Yii::$app->getErrorHandler()->exception) === null) {
            $exception = new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }
        if ($exception instanceof \HttpException) {
            Yii::$app->response->setStatusCode($exception->getCode());
        } elseif($exception instanceof HttpException) {
            return ['error' => $exception->getMessage(), 'code' => $exception->statusCode];
        } else {
            Yii::$app->response->setStatusCode(500);
        }
        return ['error' => $exception->getMessage(), 'code' => $exception->getCode()];
    }
}
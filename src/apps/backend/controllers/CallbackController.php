<?php

namespace code2magic\baseApp\backend\controllers;

use werewolf8904\cmscore\controllers\BackendController;
use code2magic\baseApp\backend\models\search\CallbackSearch;
use code2magic\baseApp\models\Callback;


/**
 * ArticleController implements the CRUD actions for Article model.
 */
class CallbackController extends BackendController
{
    public $searchClass = CallbackSearch::class;


    public $class = Callback::class;
}

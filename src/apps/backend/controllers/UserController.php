<?php

namespace code2magic\baseApp\backend\controllers;

use code2magic\baseApp\backend\models\search\UserSearch;
use code2magic\baseApp\backend\models\User;
use code2magic\baseApp\backend\models\UserForm;
use code2magic\catalog\backend\models\Product;
use code2magic\catalog\models\PromotionCart;
use code2magic\checkout\backend\models\Order;
use code2magic\checkout\frontend\components\cart\ShoppingCartWithDb;
use code2magic\baseApp\models\UserWishlist;
use werewolf8904\cmscore\controllers\BackendController;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends BackendController
{
    public $layout = 'common';

    /** @inheritDoc */
    public function actions()
    {
        $actions = parent::actions();
        $actions['list'] = [
            'class' => \code2magic\core\web\actions\Select2Action::class,
            'getQuery' => function ($words) {
                return User::find()
                    ->joinWith(['userProfile p'])
                    ->andFilterWhere(['or',
                        ['like', 'username', $words],
                        ['like', 'email', $words],
                        ['or like', 'p.firstname', $words],
                        ['or like', 'p.middlename', $words],
                        ['or like', 'p.lastname', $words],
                    ]);
            },
            'prepareText' => 'backendName',
        ];
        return $actions;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post',],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', compact('searchModel', 'dataProvider'));
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $this->view->params['returnUrl'] = $this->getReturn();
        return $this->render('view', [
            'model' => $this->findModel($id)
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::find()->andWhere(['id' => $id])->one()) !== null) {
            if (Yii::$app->user->can('administrator')) {
                return $model;
            }
            if (\count(array_intersect_key(Yii::$app->authManager->getRolesByUser($model->id), ['administrator' => 'administrator', 'manager' => 'manager'])) < 1) {
                return $model;
            }
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $this->view->params['returnUrl'] = $this->getReturn();
        $model = new UserForm();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->keyStorage->get('backend.stay_after_save', true) || array_key_exists('stay', Yii::$app->request->post())) {
                Yii::$app->session->setFlash('alert', [
                    'body' => Yii::t('backend', 'Successfully saved'),
                    'options' => ['class' => 'alert alert-success']
                ]);

            } else {
                return $this->goReturn();
            }
        }
        return $this->render('create', [
            'model' => $model,
            'orders' => [],
            'wishlists' => [],
            'cart_positions' => [],
            'availablePromotionCart' => [],
            'usedPromotionCart' => [],
            'roles' => $this->getRoles(),
        ]);
    }

    /**
     * @return array
     */
    protected function getRoles()
    {
        if (Yii::$app->user->can('administrator')) {
            $roles = Yii::$app->authManager->getRoles();
        } else {
            $roles = [];
        }
        return ArrayHelper::map($roles, 'name', function ($array) {
            return $array->name . ' ' . $array->description;
        });
    }

    /**
     * Updates an existing User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $this->view->params['returnUrl'] = $this->getReturn();
        $model = new UserForm();
        $model->setModel($this->findModel($id));
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (
                Yii::$app->keyStorage->get('backend.stay_after_save', true)
                || array_key_exists('stay', Yii::$app->request->post())
            ) {
                Yii::$app->session->setFlash('alert', [
                    'body' => Yii::t('backend', 'Successfully saved'),
                    'options' => ['class' => 'alert alert-success']
                ]);
                return $this->refresh('#' . Yii::$app->request->post($this->stay_post_parameter));
            }
            return $this->goReturn();
        }
        $orders = Order::find()->andWhere(['user_id' => $model->id,])->all();
        $cart = new ShoppingCartWithDb(['user' => $model->getModel()->userProfile,]);
        $cart_positions = $cart->positions;
        $wishlists = UserWishlist::find()->andWhere(['user_id' => $model->id])->with(['product' => function ($query) {
            $query->modelClass = Product::class;
            return $query;
        },])->all();
        $availablePromotionCart = PromotionCart::find()
            ->available(false, false)
            ->used(false)->all();
        $usedPromotionCart = PromotionCart::find()
            ->available(false, false)
            ->used(true)->all();
        return $this->render('update', ArrayHelper::merge(
            compact('model', 'orders', 'wishlists', 'cart_positions', 'availablePromotionCart', 'usedPromotionCart'),
            [
                'roles' => $this->getRoles(),
            ]
        ));
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        Yii::$app->authManager->revokeAll($id);
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }
}

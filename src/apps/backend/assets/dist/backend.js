$(function () {
    "use strict";

    $(document).on('change', '[type=checkbox]', function (e) {
        var ids = $('.grid-view').yiiGridView('getSelectedRows');
        if (ids.length > 0) {
            $('.bulk-button').attr('disabled', false);
        } else {
            $('.bulk-button').attr('disabled', true);
        }
    });
    $(document).on('click', '.bulk-action a', function (e) {
        var ids = $('.grid-view').yiiGridView('getSelectedRows');
        // debugger;
        var parent = $('.bulk-button');
        var _this = $(this).parent();
        // debugger;
        var url = parent.data('url');
        var data = _this.data('data');
        var action = _this.data('action');
        var sender = function () {
            $.ajax({
                type: 'POST',
                url: url,
                data: {"ids": ids, 'data': data, 'action': action},
                proccessData: false,
                success: function (data) {
                    if (data) {
                        var pjax_container = $('[data-pjax-container]');
                        var id = pjax_container.attr('id');
                        $.pjax.reload({container: '#' + id});
                    }
                }
            });
        };
        yii.confirm(_this.data('message'), sender);
        return false;
    });
    /**
     * open tab
     * @type {string}
     */
    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }
    //Make the dashboard widgets sortable Using jquery UI
    $(".connectedSortable").sortable({
        placeholder: "sort-highlight",
        connectWith: ".connectedSortable",
        handle: ".box-header, .nav-tabs",
        forcePlaceholderSize: true,
        zIndex: 999999
    }).disableSelection();
    $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

    //CKEDITOR.dtd.$removeEmpty.i = 0;
    //CKEDITOR.dtd.$removeEmpty['span'] = false;

    var global_images = [];

    // input_preview
    function input_preview_init(element) {
        element = typeof element != 'undefined' ? element : 'body';
        $(element).find('.input_preview').each(function (index) {
            if (typeof $(this).data('img') == 'undefined') {
                var _this = $(this);
                if (!_this.val()) {
                    var im = global_images.shift();
                    if (im) {
                        _this.val(im.replace(_this.data('storageSrc'), ''));
                    }

                }
                var img = $('<img/>', {
                    src: (_this.data('storageSrc') && _this.val().substr(0, 4) != 'http' ? _this.data('storageSrc') : '') + (_this.val() ? _this.val() : _this.data('defaultSrc')),
                    class: 'img-thumbnail',
                    width: 150
                });

                _this.data('img', img);
                _this.parent().after(img);
                _this.change(function () {
                    _this.val(_this.val().replace(_this.data('storageSrc'), ''));

                    img.attr('src', (_this.data('storageSrc') && _this.val().substr(0, 4) != 'http' ? _this.data('storageSrc') : '') + (_this.val() ? _this.val() : _this.data('defaultSrc')));
                    var val = _this.val();
                    if (val.indexOf(', ') !== -1) {
                        var images = val.split(', ');
                        _this.val(images.shift());

                        global_images = images;
                        var count = global_images.length;
                        for (var i = 0; i < count; i++) {
                            _this.parent().parent().parent().parent().parent().parent().find('.js-input-plus').trigger('click');
                        }
                        _this.trigger('change');
                    }

                });
            }
        });
    }

    input_preview_init();
    $(document).on('afterAddRow', '.multiple-input', function (event) {
        input_preview_init(event.target);
    });

    //main cahckbox
    $('.multiple-input').on('click', '[data-is_main]',
        function (e) {
            var mul = $(e.delegateTarget);
            var radios = mul.find('[data-is_main]');
            $.each(radios, function (index, value) {
                // debugger;
                value.value = 0;
                value.checked = false;
            });
            this.value = 1;
            this.checked = true;
            return true;
        });
    $(document).on('afterDeleteRow', '.multiple-input', function (event, row) {
        var mul = $(row);
        var radios = mul.find('[data-is_main]');
        if (radios.val() == 1) {
            var mi = $(event.currentTarget).find('[data-is_main]:first');
            if (mi.length > 0) {
                mi[0].checked = true;
                mi[0].value = 1;
            }
        }
        input_preview_init(event.target);
    });
    $(document).on('afterAddRow', '.multiple-input', function (event, row) {
        var mul = $(row);
        // debugger;
        var radios = mul.find('[data-is_main]');
        var mi = $(event.currentTarget).find('[data-is_main]');
        // debugger;
        if (mi.length > 1) {
            $.each(radios, function (index, value) {
                value.value = 0;
                value.checked = false;
            });
        } else {
            $.each(radios, function (index, value) {
                value.value = 1;
                value.checked = true;
            });
        }
    });
});

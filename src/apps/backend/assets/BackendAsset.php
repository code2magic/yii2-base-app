<?php

namespace code2magic\baseApp\backend\assets;

use yii\web\AssetBundle;

/**
 * Class BackendAsset
 * @package code2magic\baseApp\backend\assets
 */
class BackendAsset extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $sourcePath = __DIR__ . '/dist';

    /**
     * @inheritDoc
     */
    public $css = [
        'backend.css',
    ];

    /**
     * @inheritDoc
     */
    public $js = [
        'backend.js',
    ];

    /**
     * @inheritDoc
     */
    public $depends = [
        \code2magic\core\web\assets\Html5shiv::class,
        \yii\web\YiiAsset::class,
        \code2magic\baseApp\backend\assets\AdminLte::class,
    ];
}

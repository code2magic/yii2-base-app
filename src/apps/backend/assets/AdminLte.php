<?php

namespace code2magic\baseApp\backend\assets;

use yii\web\AssetBundle;

/**
 * Class AdminLte
 * @package code2magic\baseApp\assets
 */
class AdminLte extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $sourcePath = '@npm/admin-lte/dist';

    /**
     * @inheritDoc
     */
    public $js = [
        'js/adminlte.min.js',
    ];

    /**
     * @inheritDoc
     */
    public $css = [
        'css/AdminLTE.min.css',
        'css/skins/_all-skins.min.css',
    ];

    /**
     * @inheritDoc
     */
    public $depends = [
        \code2magic\core\web\assets\FontAwesome::class,
        \common\assets\JquerySlimScroll::class,
        \yii\web\JqueryAsset::class,
        \yii\jui\JuiAsset::class,
        \yii\bootstrap\BootstrapPluginAsset::class,
    ];
}

<?php

namespace code2magic\baseApp\backend\modules\file\controllers;

use alexantr\elfinder\CKEditorAction;
use alexantr\elfinder\ConnectorAction;
use alexantr\elfinder\InputFileAction;
use Yii;
use yii\web\Controller;

class ManagerController extends Controller
{
    /** @var string */
    public $layout = '//clear';

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'connector' => [
                'class' => ConnectorAction::class,
                'options' => [
                    'disabledCommands' => ['netmount'],
                    'connectOptions' => [
                        'filter',
                    ],
                    'bind' => [
                        'upload.pre mkdir.pre mkfile.pre rename.pre archive.pre ls.pre' => [
                            'Plugin.Normalizer.cmdPreprocess',
                            'Plugin.Sanitizer.cmdPreprocess'
                        ],
                        'ls' => [
                            'Plugin.Normalizer.cmdPostprocess',
                            'Plugin.Sanitizer.cmdPostprocess'
                        ],
                        'upload.presave' => [
                            'Plugin.AutoResize.onUpLoadPreSave',
                            'Plugin.Normalizer.onUpLoadPreSave',
                            'Plugin.Sanitizer.onUpLoadPreSave'
                        ]

                    ],
                    'plugin' => [
                        'Sanitizer' => [
                            'enable' => Yii::$app->keyStorage->get('backend.image_enable_sanitizer', true),
                            'targets' => ['\\', '/', ':', '*', '?', '"', '<', '>', '|', '(', ')', '+', ' '], // target chars
                            'replace' => ''    // replace to this
                        ],
                        'Normalizer' => [
                            'enable' => Yii::$app->keyStorage->get('backend.image_enable_normalizer', true),
                            'nfc' => true,
                            'nfkc' => true,
                            'umlauts' => true,
                            'lowercase' => true,
                            'convmap' => require Yii::getAlias('@backend/config/_convmap.php'),
                        ],
                        'AutoResize' => [
                            'enable' => Yii::$app->keyStorage->get('backend.image_autoresize', false),
                            'maxWidth' => Yii::$app->keyStorage->get('backend.image_autoresize_width', 1500),
                            'maxHeight' => Yii::$app->keyStorage->get('backend.image_autoresize_height', 1500),
                            'quality' => Yii::$app->keyStorage->get('backend.image_autoresize_quality', 70),
                        ]
                    ],
                    'roots' => [
                        [
                            'driver' => 'LocalFileSystem',
                            'path' => Yii::getAlias('@storage/web/source'),
                            'URL' => Yii::getAlias('@storageUrl/source'),
                            'alias' => Yii::t('backend', 'Storage'),
                            'uploadDeny' => [
                                //'text/x-php', 'text/php', 'application/x-php', 'application/php'
                            ],
                        ],
                        [
                            'driver' => 'LocalFileSystem',
                            'path' => Yii::getAlias('@frontend/web'),
                            'URL' => Yii::getAlias('@frontendUrl'),
                            'alias' => Yii::t('backend', 'Frontend'),
                            'uploadDeny' => [
                                //'text/x-php', 'text/php', 'application/x-php', 'application/php'
                            ],
                        ],
                    ],
                ],
            ],
            'input' => [
                'class' => InputFileAction::class,
                'connectorRoute' => 'connector',
                'separator' => ',',
                'textareaSeparator' => '\n',
            ],
            //'folder-input' => [
            //    'class' => InputFileAction::class,
            //    'connectorRoute' => 'connector',
            //    'separator' => ',',
            //    'textareaSeparator' => '\n',
            //    'settings' => [
            //        'commandsOptions' => [
            //            'getfile' => [
            //                'folders' => true,
            //            ],
            //        ],
            //    ],
            //],
            'ckeditor' => [
                'class' => CKEditorAction::class,
                'connectorRoute' => 'connector',
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}

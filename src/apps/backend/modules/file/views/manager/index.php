<?php

/**
 * @var $this yii\web\View
 */

$this->title = Yii::t('backend', 'File Manager');

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <div class="col-xs-12">
        <?= \alexantr\elfinder\ElFinder::widget([
            'connectorRoute' => ['connector'],
            'settings' => [
                'height' => '600px',
                'width' => '100%',
            ],
            'buttonNoConflict' => true,
        ]) ?>
    </div>
</div>

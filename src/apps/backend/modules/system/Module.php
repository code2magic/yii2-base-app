<?php

namespace code2magic\baseApp\backend\modules\system;

/**
 * system module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'code2magic\baseApp\backend\modules\system\controllers';
}

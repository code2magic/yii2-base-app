<?php

namespace code2magic\baseApp\backend\assets;

use yii\web\AssetBundle;

/**
 * Class BackendAsset
 * @package code2magic\baseApp\backend\assets
 */
class InformationAsset extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $sourcePath = __DIR__ . '/dist';

    /**
     * @inheritDoc
     */
    public $css = [
        //'information.css',
    ];

    /**
     * @inheritDoc
     */
    public $js = [
        'information.js',
    ];

    /**
     * @inheritDoc
     */
    public $depends = [
        \code2magic\core\web\assets\Html5shiv::class,
        \yii\web\YiiAsset::class,
        \code2magic\baseApp\backend\assets\AdminLte::class,
    ];
}

<?php
/**
 * @var $this \yii\web\View
 * @var $model \code2magic\baseApp\components\keyStorage\FormModel
 */

use code2magic\baseApp\components\keyStorage\FormWidget;

$this->title = Yii::t('backend', 'Application settings');

?>
<?= FormWidget::widget([
    'model' => $model,
    'formClass' => \yii\bootstrap\ActiveForm::class,
    'submitText' => Yii::t('backend', 'Save'),
    'submitOptions' => ['class' => 'btn btn-primary'],
]) ?>

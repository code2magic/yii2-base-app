<?php
/**
 * @var \yii\web\View $this
 * @var \code2magic\baseApp\components\keyStorage\models\KeyStorageItem $model
 * @var \yii\bootstrap\ActiveForm $form
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
]) ?>
<?= $form->field($model, 'key')->textInput() ?>
<?= $form->field($model, 'value')->textInput() ?>
<?= $form->field($model, 'comment')->textarea() ?>
<div class="form-group">
    <?= Html::submitButton(
        $model->isNewRecord
            ? Yii::t('backend', 'Create')
            : Yii::t('backend', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',]
    ) ?>
</div>
<?php ActiveForm::end() ?>

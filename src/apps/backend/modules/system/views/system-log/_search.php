<?php
/**
 * @var $this yii\web\View
 * @var $model \code2magic\baseApp\backend\modules\system\models\search\SystemLogSearch
 * @var $form yii\bootstrap\ActiveForm
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="system-log-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get'
    ]); ?>
    <?= $form->field($model, 'id') ?>
    <?= $form->field($model, 'level') ?>
    <?= $form->field($model, 'category') ?>
    <?= $form->field($model, 'log_time') ?>
    <?= $form->field($model, 'prefix') ?>
    <?= $form->field($model, 'message') ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

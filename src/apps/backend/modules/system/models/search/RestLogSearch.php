<?php

namespace code2magic\baseApp\backend\modules\system\models\search;



use code2magic\baseApp\backend\modules\system\models\RestLog;

/**
 * SystemLogSearch represents the model behind the search form about `code2magic\baseApp\backend\modules\log\models\SystemLog`.
 */
class RestLogSearch extends SystemLogSearch
{
    public $parent = RestLog::class;

}

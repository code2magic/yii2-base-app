<?php

namespace code2magic\baseApp\backend\modules\system\models\search;

use code2magic\baseApp\backend\modules\system\models\SystemLog;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SystemLogSearch represents the model behind the search form about `backend\models\SystemLog`.
 */
class SystemLogSearch extends SystemLog
{
    public $parent = SystemLog::class;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'log_time'], 'integer'],
            [ 'message','string'],
            [['category', 'prefix', 'level'], 'safe']
        ];
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->parent::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['log_time' => SORT_DESC]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'level' => $this->level,
            'log_time' => $this->log_time,

        ]);
        $query->andFilterWhere([
            'like','message',$this->message
        ]);

        $query->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'prefix', $this->prefix]);

        return $dataProvider;
    }
}

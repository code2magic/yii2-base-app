<?php

namespace code2magic\baseApp\backend\modules\system\models;


/**
 * This is the model class for table "system_log".
 *

 */
class RestLog extends SystemLog
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rest_log}}';
    }


}
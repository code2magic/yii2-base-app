<?php

namespace code2magic\baseApp\backend\modules\system\controllers;

use code2magic\baseApp\backend\widgets\AceEditor;
use code2magic\baseApp\components\keyStorage\FormModel;
use code2magic\checkout\backend\models\OrderStatus;
use code2magic\checkout\shipmentApi\novaPoshta\NovaPoshtaApi;
use code2magic\checkout\shipmentApi\ukrPost\UkrPoshtaInvoice;
use code2magic\checkout\shipmentApi\ukrPost\UkrPostApi;
use code2magic\payment\Status;
use code2magic\baseApp\web\assets\NestedSelect2Asset;
use kartik\widgets\Select2;
use werewolf8904\cmscontent\backend\models\ArticleCategory;
use werewolf8904\cmscore\language\ILanguage;
use werewolf8904\cmsdbwidgets\links\IUrlData;
use Yii;
use yii\di\Instance;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * Class SettingsController
 * @package code2magic\baseApp\backend\modules\system\controllers
 */
class SettingsController extends Controller
{
    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $schedule_columns = [
            [
                'title' => Yii::t('backend', 'Schedule Time'),
                'name' => 'schedule_time',
            ],
        ];
        $npApi = Instance::ensure(NovaPoshtaApi::class);
        $ukrApi = Instance::ensure(UkrPostApi::class);
        $data = Yii::$container->get(IUrlData::class)->getDataArray();
        $data_articles_category = ArrayHelper::map(ArticleCategory::find()->published()->all(), 'id', 'name');
        $urlManagerApi = \yii\di\Instance::ensure('urlManager-api');
        /**
         * @var $language ILanguage
         */
        $language = Instance::ensure(ILanguage::class);
        foreach ($language->getLanguages() as $code => $name) {
            $schedule_columns[] = [
                'title' => Yii::t('backend', 'Schedule Info') . ' ' . $name,
                'name' => 'schedule_' . $code,
            ];
        }
        $model = new FormModel([
            'keys' => [
                /* Contact data */
                'common.shop_name' => [
                    'tab' => Yii::t('backend', 'Contact data'),
                    'label' => Yii::t('backend', 'Shop Name'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                    'widget' => \yii\bootstrap\InputWidget::class,
                ],
                'common.admin_emails' => [
                    'tab' => Yii::t('backend', 'Contact data'),
                    'label' => Yii::t('backend', 'Admin emails'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => \unclead\multipleinput\MultipleInput::class,
                    'options' => [
                        'min' => 0,
                        'sortable' => true,
                        'attribute' => 'value',
                        'columns' => [
                            [
                                'title' => Yii::t('backend', 'E-mail'),
                                'name' => 'email',
                            ],
                            [
                                'title' => Yii::t('backend', 'Name'),
                                'name' => 'name',
                            ],
                        ],
                    ],
                ],
                'frontend.contact_phones' => [
                    'tab' => Yii::t('backend', 'Contact data'),
                    'label' => Yii::t('backend', 'Contact phones'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => \unclead\multipleinput\MultipleInput::class,
                    'options' => [
                        'sortable' => true,
                        'attribute' => 'value',
                        'columns' => [
                            [
                                'type' => \borales\extensions\phoneInput\PhoneInput::class,
                                'title' => Yii::t('backend', 'Phone'),
                                'name' => 'phone',
                                'options' => [
                                    'jsOptions' => [
                                        'onlyCountries' => ['ua',],
                                        'nationalMode' => false,
                                        //'formatOnDisplay' => false,
                                    ],
                                ],
                            ],
                            [
                                'title' => Yii::t('backend', 'Label'),
                                'name' => 'label'
                            ],
                        ],
                    ],
                ],
                'frontend.working_hours' => [
                    'tab' => Yii::t('backend', 'Contact data'),
                    'label' => Yii::t('backend', 'Working hours'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => \unclead\multipleinput\MultipleInput::class,
                    'options' => [
                        'sortable' => true,
                        'attribute' => 'value',
                        'columns' => [
                            [
                                'title' => Yii::t('backend', 'Line'),
                                'name' => 'line',
                            ],
                        ]
                    ],
                ],
                /* custom codes */
                'frontend.header_code' => [
                    'tab' => Yii::t('backend', 'Custom code'),
                    'label' => Yii::t('backend', 'Header Code'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => AceEditor::class,
                ],
                'frontend.body_start_code' => [
                    'tab' => Yii::t('backend', 'Custom code'),
                    'label' => Yii::t('backend', 'Body Start Code'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => AceEditor::class,
                ],
                'frontend.body_end_code' => [
                    'tab' => Yii::t('backend', 'Custom code'),
                    'label' => Yii::t('backend', 'Body End Code'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => AceEditor::class,
                ],
                /* cart */
                'frontend.max_cart' => [
                    'tab' => Yii::t('backend', 'Cart'),
                    'label' => Yii::t('backend', 'Max cart value'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                    'widget' => \yii\bootstrap\InputWidget::class,
                    'options' => [
                        'type' => 'number',
                    ],
                ],
                'frontend.min_cart' => [
                    'tab' => Yii::t('backend', 'Cart'),
                    'label' => Yii::t('backend', 'Min cart value'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                    'options' => [
                        'type' => 'number',
                    ],
                    'widget' => \yii\bootstrap\InputWidget::class,
                ],
                /* social */
                'frontend.group-url-pinterest' => [
                    'tab' => Yii::t('backend', 'Socials'),
                    'label' => Yii::t('backend', 'Url to {0} group', ['Pinterest',]),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'frontend.group-url-facebook' => [
                    'tab' => Yii::t('backend', 'Socials'),
                    'label' => Yii::t('backend', 'Url to {0} group', ['Facebook',]),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'frontend.group-url-instagram' => [
                    'tab' => Yii::t('backend', 'Socials'),
                    'label' => Yii::t('backend', 'Url to {0} group', ['Instagram',]),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'frontend.group-url-google' => [
                    'tab' => Yii::t('backend', 'Socials'),
                    'label' => Yii::t('backend', 'Url to {0} group', ['Google',]),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'frontend.group-url-youtube' => [
                    'tab' => Yii::t('backend', 'Socials'),
                    'label' => Yii::t('backend', 'Url to {0} group', ['YouTube',]),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'frontend.group-url-twitter' => [
                    'tab' => Yii::t('backend', 'Socials'),
                    'label' => Yii::t('backend', 'Url to {0} group', ['Twitter',]),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                /* image */
                'backend.image_autoresize' => [
                    'tab' => Yii::t('backend', 'Images'),
                    'label' => Yii::t('backend', 'Enable autoresize loaded image'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => \kartik\widgets\SwitchInput::class,
                    'options' => [
                        'type' => \kartik\widgets\SwitchInput::CHECKBOX,
                    ],
                ],
                'backend.image_autoresize_width' => [
                    'tab' => Yii::t('backend', 'Images'),
                    'label' => Yii::t('backend', 'Autoresize Width'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'backend.image_autoresize_height' => [
                    'tab' => Yii::t('backend', 'Images'),
                    'label' => Yii::t('backend', 'Autoresize Height'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'backend.image_autoresize_quality' => [
                    'tab' => Yii::t('backend', 'Images'),
                    'label' => Yii::t('backend', 'Autoresize Quality'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'backend.image_enable_sanitizer' => [
                    'tab' => Yii::t('backend', 'Images'),
                    'label' => Yii::t('backend', 'Enable sanitizer loaded image'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => \kartik\widgets\SwitchInput::class,
                    'options' => [
                        'type' => \kartik\widgets\SwitchInput::CHECKBOX,
                    ],
                ],
                'backend.image_enable_normalizer' => [
                    'tab' => Yii::t('backend', 'Images'),
                    'label' => Yii::t('backend', 'Enable normalize loaded image'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => \kartik\widgets\SwitchInput::class,
                    'options' => [
                        'type' => \kartik\widgets\SwitchInput::CHECKBOX,
                    ],
                ],
                /* admin panel */
                'backend.stay_after_save' => [
                    'tab' => Yii::t('backend', 'Admin panel'),
                    'label' => Yii::t('backend', 'Stay on page after save'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => \kartik\widgets\SwitchInput::class,
                    'options' => [
                        'type' => \kartik\widgets\SwitchInput::CHECKBOX,
                    ],
                ],
                'backend.menu_badge_order_status' => [
                    'tab' => Yii::t('backend', 'Admin panel'),
                    'label' => Yii::t('backend', 'Order status for menu badge'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => Select2::class,
                    'options' => [
                        'data' => \yii\helpers\ArrayHelper::map(
                            OrderStatus::find()->all(),
                            'id',
                            'name'
                        ),
                        'options' => [
                            'placeholder' => '',
                            'prompt' => '-',
                        ],
                        'pluginOptions' => [
                            'allowClear' => false,
                        ],
                    ],
                ],
                'backend.theme-skin' => [
                    'tab' => Yii::t('backend', 'Admin panel'),
                    'label' => Yii::t('backend', 'Backend theme'),
                    'type' => FormModel::TYPE_DROPDOWN,
                    'items' => [
                        'skin-blue' => 'skin-blue',
                        'skin-blue-light' => 'skin-blue-light',
                        'skin-yellow' => 'skin-yellow',
                        'skin-yellow-light' => 'skin-yellow-light',
                        'skin-green' => 'skin-green',
                        'skin-green-light' => 'skin-green-light',
                        'skin-purple' => 'skin-purple',
                        'skin-purple-light' => 'skin-purple-light',
                        'skin-red' => 'skin-red',
                        'skin-red-light' => 'skin-red-light',
                        'skin-black' => 'skin-black',
                        'skin-black-light' => 'skin-black-light',
                    ],
                ],
                'backend.layout-fixed' => [
                    'tab' => Yii::t('backend', 'Admin panel'),
                    'label' => Yii::t('backend', 'Fixed backend layout'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => \kartik\widgets\SwitchInput::class,
                    'options' => [
                        'type' => \kartik\widgets\SwitchInput::CHECKBOX,
                    ],
                ],
                'backend.layout-boxed' => [
                    'tab' => Yii::t('backend', 'Admin panel'),
                    'label' => Yii::t('backend', 'Boxed backend layout'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => \kartik\widgets\SwitchInput::class,
                    'options' => [
                        'type' => \kartik\widgets\SwitchInput::CHECKBOX,
                    ],
                ],
                'backend.layout-collapsed-sidebar' => [
                    'tab' => Yii::t('backend', 'Admin panel'),
                    'label' => Yii::t('backend', 'Backend sidebar collapsed'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => \kartik\widgets\SwitchInput::class,
                    'options' => [
                        'type' => \kartik\widgets\SwitchInput::CHECKBOX,
                    ],
                ],
                /* Frontend */
                //'frontend.filters_if_empty' => [
                //    'tab' => Yii::t('backend', 'Frontend'),
                //    'label' => Yii::t('backend', 'Show all filters for category, if no added'),
                //    'type' => FormModel::TYPE_WIDGET,
                //    'options' => [
                //        'type' => \kartik\widgets\SwitchInput::CHECKBOX,
                //    ],
                //    'widget' => \kartik\widgets\SwitchInput::class,
                //],
                'frontend.catalog_products' => [
                    'tab' => Yii::t('backend', 'Frontend'),
                    'label' => Yii::t('backend', 'Frontend catalog products'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'frontend.maintenance' => [
                    'tab' => Yii::t('backend', 'Frontend'),
                    'label' => Yii::t('backend', 'Frontend maintenance mode'),
                    'type' => FormModel::TYPE_DROPDOWN,
                    'items' => [
                        0 => Yii::t('backend', 'Disabled'),
                        1 => Yii::t('backend', 'Enabled'),
                    ],
                ],
                /* checkout */
                /*// Payment */
                'payment.order_status_map' => [
                    'tab' => Yii::t('backend', 'Payment'),
                    'label' => Yii::t('backend', 'Payment to Order statuses map'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => \unclead\multipleinput\MultipleInput::class,
                    'options' => [
                        'attribute' => 'value',
                        'columns' => [
                            [
                                'title' => Yii::t('backend', 'Payment status'),
                                'name' => 'payment_status',
                                'type' => \kartik\widgets\Select2::class,
                                'options' => [
                                    'data' => Status::listData(),
                                ],
                            ],
                            [
                                'title' => Yii::t('backend', 'Order status'),
                                'name' => 'order_status',
                                'type' => \kartik\widgets\Select2::class,
                                'enableError' => true,
                                'options' => [
                                    'data' => \yii\helpers\ArrayHelper::map(OrderStatus::find()->all(), 'id', 'name'),
                                    'options' => [
                                        'placeholder' => '',
                                        'prompt' => '-',
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false,
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                /*// Shipment */
                /*//// Nova Poshta */
                'order.np.firstname' => [
                    'tab' => Yii::t('backend', 'Nova Poshta'),
                    'label' => Yii::t('backend', 'Firstname'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'order.np.middlename' => [
                    'tab' => Yii::t('backend', 'Nova Poshta'),
                    'label' => Yii::t('backend', 'Middlename'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'order.np.lastname' => [
                    'tab' => Yii::t('backend', 'Nova Poshta'),
                    'label' => Yii::t('backend', 'Lastname'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'order.np.phone' => [
                    'tab' => Yii::t('backend', 'Nova Poshta'),
                    'label' => Yii::t('backend', 'Phone'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'order.np.area' => [
                    'tab' => Yii::t('backend', 'Nova Poshta'),
                    'label' => Yii::t('backend', 'Area'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => \kartik\widgets\Select2::class,
                    'options' => [
                        'data' => $npApi->getAreas(),
                        'options' => [
                            'id' => 'nova_poshta_area',
                            'placeholder' => '',
                            'prompt' => '-',
                            'data' => [
                                'child_element' => '#nova_poshta_city',
                            ],
                        ],
                        'pluginOptions' => [
                            'ajax' => [
                                'url' => $urlManagerApi->createAbsoluteUrl([
                                    '/checkout/shipment/nova-poshta',
                                    'type' => \code2magic\checkout\shipmentApi\novaPoshta\NovaPoshtaApi::AREAS,
                                ]),
                                'delay' => 500,
                            ],
                        ],
                    ],
                ],
                'order.np.city' => [
                    'tab' => Yii::t('backend', 'Nova Poshta'),
                    'label' => Yii::t('backend', 'City'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => \kartik\widgets\Select2::class,
                    'options' => [
                        'data' => Yii::$app->keyStorage->get('order.np.area') ? $npApi->getCities(Yii::$app->keyStorage->get('order.np.area')) : [],
                        'options' => [
                            'id' => 'nova_poshta_city',
                            'placeholder' => '',
                            'prompt' => '-',
                            'data' => [
                                'parent_element' => '#nova_poshta_area',
                                //'parent_param' => 'area_id',
                                'child_element' => '#nova_poshta_warehouse',
                            ],
                        ],
                        'pluginOptions' => [
                            'ajax' => [
                                'url' => $urlManagerApi->createAbsoluteUrl([
                                    '/checkout/shipment/nova-poshta',
                                    'type' => \code2magic\checkout\shipmentApi\novaPoshta\NovaPoshtaApi::CITIES,
                                ]),
                                'data' => NestedSelect2Asset::getSelect2DataFunction(),
                                'delay' => 500,
                            ],
                        ],
                    ],
                ],
                'order.np.warehouse' => [
                    'tab' => Yii::t('backend', 'Nova Poshta'),
                    'label' => Yii::t('backend', 'Warehouse'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => \kartik\widgets\Select2::class,
                    'options' => [
                        'data' => Yii::$app->keyStorage->get('order.np.city') ? $npApi->getWarehouses(Yii::$app->keyStorage->get('order.np.city')) : [],
                        'options' => [
                            'id' => 'nova_poshta_warehouse',
                            'placeholder' => '',
                            'prompt' => '-',
                            'data' => [
                                'parent_element' => '#nova_poshta_city',
                                //'parent_param' => 'area_id',
                            ],
                        ],
                        'pluginOptions' => [
                            'ajax' => [
                                //'url' => \yii\di\Instance::ensure('urlManager-api')->createAbsoluteUrl([$api_route,]),
                                'url' => $urlManagerApi->createAbsoluteUrl([
                                    '/checkout/shipment/nova-poshta',
                                    'type' => \code2magic\checkout\shipmentApi\novaPoshta\NovaPoshtaApi::WAREHOUSES,
                                ]),
                                'data' => NestedSelect2Asset::getSelect2DataFunction(),
                                'delay' => 500,
                            ],
                        ],
                    ],
                ],
                /*//// Ukr Poshta */
                'order.ukrp.lastname' => [
                    'tab' => Yii::t('backend', 'Ukr Poshta'),
                    'label' => Yii::t('backend', 'Lastname'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'order.ukrp.firstname' => [
                    'tab' => Yii::t('backend', 'Ukr Poshta'),
                    'label' => Yii::t('backend', 'Firstname'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'order.ukrp.middlename' => [
                    'tab' => Yii::t('backend', 'Ukr Poshta'),
                    'label' => Yii::t('backend', 'Middlename'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'order.ukrp.phone' => [
                    'tab' => Yii::t('backend', 'Ukr Poshta'),
                    'label' => Yii::t('backend', 'Phone'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'order.ukrp.name' => [
                    'tab' => Yii::t('backend', 'Ukr Poshta'),
                    'label' => Yii::t('backend', 'Name'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'order.ukrp.type' => [
                    'tab' => Yii::t('backend', 'Ukr Poshta'),
                    'label' => Yii::t('backend', 'Type'),
                    'type' => FormModel::TYPE_DROPDOWN,
                    'items' => [
                        UkrPoshtaInvoice::CLIENT_TYPE_PRIVATE_ENTREPRENEUR => Yii::t('backend', 'Private Entrepreneur'),
                        UkrPoshtaInvoice::CLIENT_TYPE_INDIVIDUAL => Yii::t('backend', 'Individual'),
                    ],
                ],
                'order.ukrp.tin' => [
                    'tab' => Yii::t('backend', 'Ukr Poshta'),
                    'label' => Yii::t('backend', 'Tin'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'order.ukrp.index' => [
                    'tab' => Yii::t('backend', 'Ukr Poshta'),
                    'label' => Yii::t('backend', 'Index'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => \kartik\widgets\Select2::class,
                    'options' => [
                        'data' => Yii::$app->keyStorage->get('order.ukrp.index') ? $ukrApi->getWarehousesCode(Yii::$app->keyStorage->get('order.ukrp.index')) : [],
                        'options' => [
                            'id' => 'ukrpost_index',
                            'placeholder' => '',
                            'prompt' => '-',
                            'data' => [
                                'child_element' => '#ukrpost_street',
                            ],
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 5,
                            'ajax' => [
                                //'url' => \yii\di\Instance::ensure('urlManager-api')->createAbsoluteUrl([$api_route,]),
                                'url' => $urlManagerApi->createAbsoluteUrl([
                                    '/checkout/shipment/ukrposhta',
                                    'type' => \code2magic\checkout\shipmentApi\ukrPost\UkrPostApi::WAREHOUSES_CODE,
                                ]),
                                'data' => NestedSelect2Asset::getSelect2DataFunction(),
                                'delay' => 500,
                            ],
                        ],
                    ],
                ],
                'order.ukrp.street' => [
                    'tab' => Yii::t('backend', 'Ukr Poshta'),
                    'label' => Yii::t('backend', 'Street'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => \kartik\widgets\Select2::class,
                    'options' => [
                        'data' => Yii::$app->keyStorage->get('order.ukrp.index') ? $ukrApi->getStreetsByIndex(Yii::$app->keyStorage->get('order.ukrp.index')) : [],
                        'options' => [
                            'id' => 'ukrpost_street',
                            'placeholder' => '',
                            'prompt' => '-',
                            'data' => [
                                'parent_element' => '#ukrpost_index',
                                'child_element' => '#ukrpost_house',
                            ],
                        ],
                        'pluginOptions' => [
                            'ajax' => [
                                'url' => $urlManagerApi->createAbsoluteUrl([
                                    '/checkout/shipment/ukrposhta',
                                    'type' => UkrPostApi::STREETS_BY_INDEX,
                                ]),
                                'data' => NestedSelect2Asset::getSelect2DataFunction(),
                                'delay' => 500,
                            ],
                        ],
                    ],
                ],
                'order.ukrp.house' => [
                    'tab' => Yii::t('backend', 'Ukr Poshta'),
                    'label' => Yii::t('backend', 'House'),
                    'type' => FormModel::TYPE_WIDGET,
                    'widget' => \kartik\widgets\Select2::class,
                    'options' => [
                        'data' => Yii::$app->keyStorage->get('order.ukrp.street') ? $ukrApi->getHousesByStreet(Yii::$app->keyStorage->get('order.ukrp.street')) : [],
                        'options' => [
                            'id' => 'ukrpost_house',
                            'placeholder' => '',
                            'prompt' => '-',
                            'data' => [
                                'parent_element' => '#ukrpost_street',
                            ],
                        ],
                        'pluginOptions' => [
                            'ajax' => [
                                'url' => $urlManagerApi->createAbsoluteUrl([
                                    '/checkout/shipment/ukrposhta',
                                    'type' => UkrPostApi::HOUSE_BY_STREET,
                                ]),
                                'data' => NestedSelect2Asset::getSelect2DataFunction(),
                                'delay' => 500,
                            ],
                        ],
                    ],
                ],
                'order.ukrp.apartment' => [
                    'tab' => Yii::t('backend', 'Ukr Poshta'),
                    'label' => Yii::t('backend', 'Apartment'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'order.ukrp.sender.addressId' => [
                    'tab' => Yii::t('backend', 'Ukr Poshta'),
                    'label' => Yii::t('backend', 'Sender Address Id'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'order.ukrp.sender.uuid' => [
                    'tab' => Yii::t('backend', 'Ukr Poshta'),
                    'label' => Yii::t('backend', 'Sender UUID'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                //'frontend.filters_if_empty' => [
                //    'tab' => Yii::t('backend', 'frontend panel'),
                //    'label' => Yii::t('backend', 'Show all filters for category, if no added'),
                //    'type' => FormModel::TYPE_WIDGET,
                //    'widget' => \kartik\widgets\SwitchInput::class,
                //    'options' => [
                //        'type' => \kartik\widgets\SwitchInput::CHECKBOX,
                //    ],
                //],
            ],
        ]);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('alert', [
                'body' => Yii::t('backend', 'Settings was successfully saved'),
                'options' => ['class' => 'alert alert-success',],
            ]);
            return $this->refresh();
        }
        return $this->render('index', ['model' => $model,]);
    }
}

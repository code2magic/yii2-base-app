<?php
/**
 * Eugine Terentev <eugine@terentev.net>
 */

namespace code2magic\baseApp\backend\modules\system\controllers;

use Yii;
use yii\caching\Cache;
use yii\caching\TagDependency;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\HttpException;

/**
 * Class CacheController
 *
 * @package code2magic\baseApp\backend\controllers
 */
class CacheController extends Controller
{
    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        $dataProvider = new ArrayDataProvider(['allModels' => $this->findCaches()]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * Returns array of caches in the system, keys are cache components names, values are class names.
     *
     * @param array $cachesNames caches to be found
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    private function findCaches(array $cachesNames = [])
    {
        $caches = [];
        $components = Yii::$app->getComponents();
        $findAll = ($cachesNames == []);
        foreach ($components as $name => $component) {
            $component = Yii::$app->get($name);
            if (!$findAll && !in_array($name, $cachesNames)) {
                continue;
            }
            if ($component instanceof Cache) {
                $caches[$name] = ['name' => $name, 'class' => get_class($component),];
            } else if (is_array($component) && isset($component['class']) && $this->isCacheClass($component['class'])) {
                $caches[$name] = ['name' => $name, 'class' => $component['class'],];
            } else if (is_string($component) && $this->isCacheClass($component)) {
                $caches[$name] = ['name' => $name, 'class' => $component,];
            }
        }

        return $caches;
    }

    /**
     * Checks if given class is a Cache class.
     *
     * @param string $className class name.
     *
     * @return boolean
     */
    private function isCacheClass($className)
    {
        return is_subclass_of($className, Cache::class);
    }

    /**
     * @return \yii\web\Response
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function actionFlushGlide()
    {
        $exclude_paths = ['.gitignore',];
        $cache = Yii::$app->glide->getServer()->getCache();
        foreach ($cache->listContents('', false) as $item) {
            if (in_array($item['path'], $exclude_paths, true)) {
                continue;
            }
            if ($item['type'] === 'dir') {
                $cache->deleteDir($item['path']);
            } else {
                $cache->delete($item['path']);
            }
        }
        Yii::$app->session->setFlash('alert', [
            'body' => \Yii::t('backend', 'Cache has been successfully flushed'),
            'options' => ['class' => 'alert-success',]
        ]);
        return $this->redirect(['index',]);
    }

    /**
     * @return \yii\web\Response
     * @throws \yii\base\ErrorException
     */
    public function actionFlushAssets()
    {
        $asset_dirs = [
            '@frontend/web/assets',
            '@backend/web/assets',
        ];
        foreach ($asset_dirs as $asset_dir) {
            if ($dir = Yii::getAlias($asset_dir, false)) {
                foreach (\yii\helpers\FileHelper::findDirectories($dir, ['recursive' => false,]) as $dir) {
                    \yii\helpers\FileHelper::removeDirectory($dir);
                }
            }
        }
        Yii::$app->session->setFlash('alert', [
            'body' => \Yii::t('backend', 'Cache has been successfully flushed'),
            'options' => ['class' => 'alert-success',]
        ]);
        return $this->redirect(['index',]);
    }


    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionFlushCache($id)
    {
        if ($this->getCache($id)->flush()) {
            Yii::$app->session->setFlash('alert', [
                'body' => \Yii::t('backend', 'Cache has been successfully flushed'),
                'options' => ['class' => 'alert-success',],
            ]);
        };
        return $this->redirect(['index',]);
    }

    /**
     * @param $id
     *
     * @return \yii\caching\Cache|null
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     */
    protected function getCache($id)
    {
        if (!in_array($id, array_keys($this->findCaches()))) {
            throw new HttpException(400, 'Given cache name is not a name of cache component');
        }
        return Yii::$app->get($id);
    }

    /**
     * @param $id
     * @param $key
     *
     * @return \yii\web\Response
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionFlushCacheKey($id, $key)
    {
        if ($this->getCache($id)->delete($key)) {
            Yii::$app->session->setFlash('alert', [
                'body' => \Yii::t('backend', 'Cache entry has been successfully deleted'),
                'options' => ['class' => 'alert-success',],
            ]);
        };
        return $this->redirect(['index',]);
    }

    /**
     * @param $id
     * @param $tag
     *
     * @return \yii\web\Response
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionFlushCacheTag($id, $tag)
    {
        TagDependency::invalidate($this->getCache($id), $tag);
        Yii::$app->session->setFlash('alert', [
            'body' => \Yii::t('backend', 'TagDependency was invalidated'),
            'options' => ['class' => 'alert-success',],
        ]);
        return $this->redirect(['index',]);
    }
}

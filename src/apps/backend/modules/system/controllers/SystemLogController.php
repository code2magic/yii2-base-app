<?php

namespace code2magic\baseApp\backend\modules\system\controllers;

use code2magic\baseApp\backend\modules\system\models\search\SystemLogSearch;
use code2magic\baseApp\backend\modules\system\models\SystemLog;
use werewolf8904\cmscore\controllers\BackendController;
use Yii;

/**
 * LogController implements the CRUD actions for SystemLog model.
 */
class SystemLogController extends BackendController
{
    public $class = SystemLog::class;
    public $searchClass = SystemLogSearch::class;

    public function actionDeleteAll()
    {
        $searchModel = new $this->searchClass();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (strcasecmp(Yii::$app->request->method, 'delete') === 0) {
            $this->class::deleteAll($dataProvider->query->where);

        }

        return $this->goReturn();
    }

    /**
     * Displays a single SystemLog model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id)
        ]);
    }
}

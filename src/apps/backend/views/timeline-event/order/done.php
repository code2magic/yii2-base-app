<?php
/**
 * @var $model code2magic\baseApp\models\TimelineEvent
 */
?>
<i class="fa fa-shopping-bag bg-green-active"></i>
<div class="timeline-item">
    <span class="time">
        <i class="fa fa-clock-o"></i>
        <?= Yii::$app->formatter->asRelativeTime($model->created_at) ?>
    </span>
    <h3 class="timeline-header">
        <?= Yii::t('backend', 'You have new order!') ?>
    </h3>
    <div class="timeline-body">
        <?= Yii::t('backend', 'New order from ({fullname}) {phone} {email} {date}', [
            'fullname' => $model->data['fullname'],
            'phone' => $model->data['phone'],
            'email' => $model->data['email'],
            'date' => Yii::$app->formatter->asDatetime($model->data['date']),
        ]) ?>
        <?php foreach ($model->data['products'] as $tovar) : ?>
            <br><?= $tovar['catalog_product_id']. ' ' .$tovar['quantity']. ' ' .$tovar['price']; ?>
        <?php endforeach; ?>
    </div>
    <div class="timeline-footer">
        <?= \yii\helpers\Html::a(
            Yii::t('backend', 'View order'),
            ['/checkout/order/update', 'id' => $model->data['id'],],
            ['class' => 'btn btn-success btn-sm',]
        ) ?>
    </div>
</div>

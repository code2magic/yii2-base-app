<?php
/**
 * @var $model code2magic\baseApp\models\TimelineEvent
 */
?>
<i class="fa fa-comments bg-yellow"></i>
<div class="timeline-item">
    <span class="time">
        <i class="fa fa-clock-o"></i>
        <?= Yii::$app->formatter->asRelativeTime($model->created_at) ?>
    </span>
    <h3 class="timeline-header">
        <?= Yii::t('backend', 'You have new comment!') ?>
    </h3>
    <div class="timeline-body">
        <?= Yii::t('backend', 'New comment from {fullname} {email} {date}', [
            'fullname' => $model->data['fullname'],
            //'phone' => $model->data['phone'],
            'email' => $model->data['email'],
            'date' => Yii::$app->formatter->asDatetime($model->data['date'])
        ]) ?>
    </div>
    <div class="timeline-footer">
        <?= \yii\helpers\Html::a(
            Yii::t('backend', 'View comment'),
          \yii\helpers\Url::to(['/comments/update','id'=>$model->data['id']]),
            ['class' => 'btn btn-success btn-sm']
        ) ?>
    </div>
</div>

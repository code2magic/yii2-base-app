<?php
/**
 * @var $model code2magic\baseApp\models\TimelineEvent
 */
?>
<i class="fa fa-envelope bg-blue"></i>
<div class="timeline-item">
    <span class="time">
        <i class="fa fa-clock-o"></i>
        <?= Yii::$app->formatter->asRelativeTime($model->created_at) ?>
    </span>
    <h3 class="timeline-header">
        <?= Yii::t('backend', 'You have new callback!') ?>
    </h3>
    <div class="timeline-body">
        <?= Yii::t('backend', 'New callback from {fullname} {phone} {date}', [
            'fullname' => $model->data['name'],
            'phone' => $model->data['phone'],
            'date' => Yii::$app->formatter->asDatetime($model->data['created_at'])
        ]) ?>
    </div>
    <div class="timeline-footer">
        <?= \yii\helpers\Html::a(
            Yii::t('backend', 'View callback'),
          \yii\helpers\Url::to(['/callback/update','id'=>$model->data['id']]),
            ['class' => 'btn btn-success btn-sm']
        ) ?>
    </div>
</div>
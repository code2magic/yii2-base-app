<?php
/**
 * @var $model code2magic\baseApp\models\TimelineEvent
 */
?><i class="fa fa-question bg-blue"></i>
<div class="timeline-item">
    <span class="time">
        <i class="fa fa-clock-o"></i>
        <?= Yii::$app->formatter->asRelativeTime($model->created_at) ?>
    </span>
    <h3 class="timeline-header">
        <?= Yii::t('backend', 'You have new question!') ?>
    </h3>
    <div class="timeline-body">
        <?= Yii::t('backend', 'New question from {fullname} {email}', [
            'fullname' => $model->data['fullname'],
            'email' => $model->data['email'],
        ]) ?>
        <p>
        <?= $model->data['question'] ?>
        </p>
    </div>
    <div class="timeline-footer">
        <?= \yii\helpers\Html::a(
            Yii::t('backend', 'View question'),
          \yii\helpers\Url::to(['/catalog/product-question/update','id'=>$model->data['id']]),
            ['class' => 'btn btn-success btn-sm']
        ) ?>
    </div>
</div>

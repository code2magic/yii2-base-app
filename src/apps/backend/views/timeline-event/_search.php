<?php
/**
 * @var $this yii\web\View
 * @var $model code2magic\baseApp\backend\models\search\TimelineEventSearch
 * @var $form yii\bootstrap\ActiveForm
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="system-event-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <?= $form->field($model, 'id') ?>
    <?= $form->field($model, 'application') ?>
    <?= $form->field($model, 'event') ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary',]) ?>
        <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default',]) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>

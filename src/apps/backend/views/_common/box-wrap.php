<?php
/**
 * @var yii\web\View $this
 * @var string $content
 */
?>
<div class="box">
    <div class="box-body">
        <?= $content ?>
    </div>
</div>

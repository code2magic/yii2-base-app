<?php
/**
 * @var yii\web\View $this
 * @var string $content
 */

use code2magic\baseApp\backend\modules\system\models\SystemLog;
use code2magic\baseApp\backend\widgets\Menu;
use code2magic\baseApp\models\TimelineEvent;
use yii\bootstrap\Alert;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\log\Logger;
use yii\widgets\Breadcrumbs;

$bundle = \backend\assets\BackendAsset::register($this);

?>
<?php $this->beginContent('@backend/views/layouts/base.php'); ?>
<div class="wrapper">
    <div class="overlay-wrapper" style="display: none">
        <div class="overlay">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
    <!-- header logo: style can be found in header.less -->
    <header class="main-header">
        <a href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl('/') ?>" class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the margining -->
            <?= Yii::$app->name ?>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" role="button" data-toggle="push-menu">
                <span class="sr-only"><?= Yii::t('backend', 'Toggle navigation') ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="navbar-left hidden-xs hidden-sm">
                <?= Menu::widget([
                    'options' => [
                        'class' => 'navbar-nav nav',
                    ],
                    'items' => [
                        [
                            'label' => Yii::t('backend', 'Orders'),
                            'url' => ['/checkout/order/index',],
                            'icon' => '<i class="fa fa-shopping-bag"></i>',
                            'active' => 'order' === \Yii::$app->controller->id,
                            'badge' => \code2magic\checkout\backend\models\Order::find()
                                ->andWhere(['checkout_order_status_id' => \Yii::$app->keyStorage->get('backend.menu_badge_order_status'),])
                                ->count(),
                            'badgeBgClass' => 'label-success',
                        ],
                        [
                            'label' => Yii::t('backend', 'Comments'),
                            'url' => ['/comments/index',],
                            'icon' => '<i class="fa fa-comments-o"></i>',
                            'active' => 'comments' === \Yii::$app->controller->id,
                            'badge' => \comments\models\CommentModel::find()->pending()->count(),
                            'badgeBgClass' => 'label-success',
                        ],
                        [
                            'label' => Yii::t('backend', 'Questions'),
                            'url' => ['/catalog/product-question/index',],
                            'icon' => '<i class="fa fa-question"></i>',
                            'visible' => Yii::$app->user->can('manager'),
                            'active' => 'product-question' === \Yii::$app->controller->id,
                            'badge' => \code2magic\catalog\backend\models\ProductQuestion::find()
                                ->andWhere(['answer' => '',])
                                ->count(),
                            'badgeBgClass' => 'label-success',
                        ],
                    ],
                ]) ?>
            </div>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <?php if (Yii::$app->user->can('administrator')): ?>
                        <li id="timeline-notifications" class="notifications-menu">
                            <a href="<?= Url::to(['/timeline-event/index',]) ?>">
                                <i class="fa fa-bell"></i>
                                <span class="label label-success">
                                    <?= TimelineEvent::find()->today()->count() ?>
                                </span>
                            </a>
                        </li>
                        <!-- Notifications: style can be found in dropdown.less -->
                        <li id="log-dropdown" class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-warning"></i>
                                <span class="label label-danger">
                                <?= SystemLog::find()->count() ?>
                            </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">
                                    <?= Yii::t('backend', 'You have {num} log items', ['num' => SystemLog::find()->count(),]) ?>
                                </li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <?php foreach (SystemLog::find()->orderBy(['log_time' => SORT_DESC])->limit(5)->all() as $logEntry): ?>
                                            <li>
                                                <a href="<?= Yii::$app->urlManager->createUrl(['/system/system-log/view', 'id' => $logEntry->id,]) ?>">
                                                    <i class="fa fa-warning <?= $logEntry->level === Logger::LEVEL_ERROR ? 'text-red' : 'text-yellow' ?>"></i>
                                                    <?= $logEntry->category ?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>
                                <li class="footer">
                                    <?= Html::a(Yii::t('backend', 'View all'), ['/system/system-log/index']) ?>
                                </li>
                            </ul>
                        </li>
                    <?php endif; ?>
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img
                                src="<?= Yii::$app->user->identity->userProfile->getAvatar($this->assetManager->getAssetUrl($bundle, 'img/anonymous.jpg')) ?>"
                                class="user-image"
                                alt="image"
                            >
                            <span>
                                <span class="user-name-desctop"><?= Yii::$app->user->identity->username ?></span>
                                <i class="caret"></i>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header light-blue">
                                <img
                                    src="<?= Yii::$app->user->identity->userProfile->getAvatar($this->assetManager->getAssetUrl($bundle, 'img/anonymous.jpg')) ?>"
                                    class="img-circle"
                                    alt="User Image"
                                />
                                <p>
                                    <?= Yii::$app->user->identity->username ?>
                                    <small>
                                        <?= Yii::t('backend', 'Member since {0, date, short}', Yii::$app->user->identity->created_at) ?>
                                    </small>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <?= Html::a(Yii::t('backend', 'Profile'), ['/sign-in/profile',], ['class' => 'btn btn-default btn-flat',]) ?>
                                </div>
                                <div class="pull-left">
                                    <?= Html::a(Yii::t('backend', 'Account'), ['/sign-in/account',], ['class' => 'btn btn-default btn-flat',]) ?>
                                </div>
                                <div class="pull-right">
                                    <?= Html::a(Yii::t('backend', 'Logout'), ['/sign-in/logout',], ['class' => 'btn btn-danger btn-flat', 'data-method' => 'post',]) ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <?php if (Yii::$app->user->can('administrator')): ?>
                        <li>
                            <?= Html::a('<i class="fa fa-cogs"></i>', ['/system/settings',]) ?>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img
                        src="<?= Yii::$app->user->identity->userProfile->getAvatar($this->assetManager->getAssetUrl($bundle, 'img/anonymous.jpg')) ?>"
                        class="img-circle"
                        alt="image"
                    />
                </div>
                <div class="pull-left info">
                    <p><?= Yii::t('backend', 'Hello, {username}', ['username' => Yii::$app->user->identity->getPublicIdentity()]) ?></p>
                    <a href="<?= Url::to(['/sign-in/profile',]) ?>">
                        <i class="fa fa-circle text-success"></i>
                        <?= Yii::$app->formatter->asDatetime(time()) ?>
                    </a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <?= Menu::widget([
                'options' => [
                    'class' => 'sidebar-menu tree',
                    'data' => ['widget' => 'tree',],
                ],
                'linkTemplate' => '<a href="{url}">{icon}<span>{label}</span>{right-icon}{badge}</a>',
                'submenuTemplate' => "\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
                'activateParents' => true,
                'items' => [
                    [
                        'label' => Yii::t('backend', 'Main'),
                        'options' => ['class' => 'header',],
                    ],
                    [
                        'label' => Yii::t('backend', 'Timeline'),
                        'icon' => '<i class="fa fa-history"></i>',
                        'url' => ['/timeline-event/index',],
                        'badge' => TimelineEvent::find()->today()->count(),
                        'badgeBgClass' => 'label-success',
                    ],
                    [
                        'label' => Yii::t('backend', 'Users'),
                        'icon' => '<i class="fa fa-users"></i>',
                        'url' => ['/user/index'],
                        'active' => Yii::$app->controller->id === 'user',
                        //'visible' => Yii::$app->user->can('administrator'),
                        'badge' => \backend\models\User::find()->today()->count(),
                        'badgeBgClass' => 'label-success',
                    ],
                    [
                        'label' => Yii::t('backend', 'Modules'),
                        'options' => ['class' => 'header',],
                    ],
                    [
                        'label' => Yii::t('backend', 'Content'),
                        'url' => '#',
                        'icon' => '<i class="fa fa-th-large"></i>',
                        'options' => ['class' => 'treeview',],
                        'active' => Yii::$app->controller->module->id === 'content',
                        'items' => [
                            [
                                'label' => Yii::t('backend', 'Static pages'),
                                'url' => ['/content/page/index'],
                                'icon' => '<i class="fa fa-thumb-tack"></i>',
                                'active' => Yii::$app->controller->id === 'page',
                            ],
                            [
                                'label' => Yii::t('backend', 'Articles'),
                                'url' => '#',
                                'icon' => '<i class="fa fa-files-o"></i>',
                                'options' => ['class' => 'treeview',],
                                'active' => in_array(Yii::$app->controller->id, ['article', 'article-category']),
                                'items' => [
                                    [
                                        'label' => Yii::t('backend', 'Articles'),
                                        'url' => ['/content/article/index'],
                                        'icon' => '<i class="fa fa-file-o"></i>',
                                        'active' => Yii::$app->controller->id === 'article',
                                    ],
                                    [
                                        'label' => Yii::t('backend', 'Categories'),
                                        'url' => ['/content/article-category/index'],
                                        'icon' => '<i class="fa fa-folder-open-o"></i>',
                                        'active' => Yii::$app->controller->id === 'article-category',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        'label' => Yii::t('backend', 'Widgets'),
                        'url' => '#',
                        'icon' => '<i class="fa fa-code"></i>',
                        'options' => ['class' => 'treeview',],
                        'active' => Yii::$app->controller->module->id === 'widget',
                        'items' => [
                            [
                                'label' => Yii::t('backend', 'Text Blocks'),
                                'url' => ['/widget/text/index',],
                                'icon' => '<i class="fa fa-circle-o"></i>',
                                'active' => Yii::$app->controller->id === 'text',
                            ],
                            [
                                'label' => Yii::t('backend', 'Composite'),
                                'url' => ['/widget/composite/index',],
                                'icon' => '<i class="fa fa-circle-o"></i>',
                                'active' => Yii::$app->controller->id === 'composite',
                            ],
                            [
                                'label' => Yii::t('backend', 'Image'),
                                'url' => ['/widget/image/index',],
                                'icon' => '<i class="fa fa-circle-o"></i>',
                                'active' => Yii::$app->controller->id === 'image',
                            ],
                            [
                                'label' => Yii::t('backend', 'Carousel'),
                                'url' => ['/widget/carousel/index',],
                                'icon' => '<i class="fa fa-circle-o"></i>',
                                'active' => in_array(Yii::$app->controller->id, ['carousel', 'carousel-item',]),
                            ],
                            [
                                'label' => Yii::t('backend', 'Menu'),
                                'url' => ['/widget/menu/index',],
                                'icon' => '<i class="fa fa-circle-o"></i>',
                                'active' => Yii::$app->controller->id === 'menu',
                            ],
                        ],
                    ],
                    //[
                    //    'label' => Yii::t('backend', 'Callback'),
                    //    'url' => ['/callback/index',],
                    //    'active' => 'callback' === \Yii::$app->controller->id,
                    //    'icon' => '<i class="fa fa-phone-square"></i>',
                    //    'visible' => Yii::$app->user->can('manager'),
                    //],
                    [
                        'label' => Yii::t('backend', 'Catalog'),
                        'url' => '#',
                        'icon' => '<i class="fa fa-shopping-bag"></i>',
                        'options' => ['class' => 'treeview',],
                        'active' => \Yii::$app->controller->module->id === 'catalog',
                        'items' => [
                            [
                                'label' => Yii::t('backend', 'Parameters'),
                                'url' => '#',
                                'icon' => '<i class="fa fa-book"></i>',
                                'options' => ['class' => 'treeview',],
                                'active' => in_array(\Yii::$app->controller->id, ['currency', 'characteristic', 'characteristic-group', 'variation-attribute', 'product-type', 'outofstock-status', 'user-customer-group', 'product-question',]),
                                'items' => [
                                    ['label' => Yii::t('backend', 'Characteristic Group'), 'url' => ['/catalog/characteristic-group/index',], 'icon' => '<i class="fa fa-minus-square"></i>', 'active' => 'characteristic-group' === \Yii::$app->controller->id,],
                                    ['label' => Yii::t('backend', 'Charateristic'), 'url' => ['/catalog/characteristic/index',], 'icon' => '<i class="fa fa-wrench"></i>', 'active' => 'characteristic' === \Yii::$app->controller->id,],
                                    ['label' => Yii::t('backend', 'Variation attribute'), 'url' => ['/catalog/variation-attribute/index',], 'icon' => '<i class="fa fa-minus-square"></i>', 'active' => 'variation-attribute' === \Yii::$app->controller->id,],
                                    ['label' => Yii::t('backend', 'Product type'), 'url' => ['/catalog/product-type/index',], 'icon' => '<i class="fa fa-minus-square"></i>', 'active' => 'product-type' === \Yii::$app->controller->id,],
                                    ['label' => Yii::t('backend', 'Currency'), 'url' => ['/catalog/currency/index'], 'icon' => '<i class="fa fa-money"></i>', 'active' => 'currency' === \Yii::$app->controller->id,],
                                    ['label' => Yii::t('backend', 'Outofstock status'), 'url' => ['/catalog/outofstock-status/index',], 'icon' => '<i class="fa fa-recycle"></i>', 'visible' => Yii::$app->user->can('manager'), 'active' => 'outofstock-status' === \Yii::$app->controller->id,],
                                    ['label' => Yii::t('backend', 'Customer group'), 'url' => ['/catalog/user-customer-group/index',], 'icon' => '<i class="fa fa-recycle"></i>', 'visible' => Yii::$app->user->can('manager'), 'active' => 'user-customer-group' === \Yii::$app->controller->id,],
                                    ['label' => Yii::t('backend', 'Questions'), 'url' => ['/catalog/product-question/index',], 'icon' => '<i class="fa fa-question"></i>', 'visible' => Yii::$app->user->can('manager'), 'active' => 'product-question' === \Yii::$app->controller->id,],
                                ],
                            ],
                            ['label' => Yii::t('backend', 'Products'), 'url' => ['/catalog/product/index'], 'icon' => '<i class="fa fa-barcode"></i>', 'active' => 'product' === \Yii::$app->controller->id,],
                            ['label' => Yii::t('backend', 'Category'), 'url' => ['/catalog/category/index'], 'icon' => '<i class="fa fa-folder-open"></i>', 'active' => 'category' === \Yii::$app->controller->id,],
                        ]
                    ],
                    [
                        'label' => Yii::t('backend', 'Checkout'),
                        'url' => '#',
                        'icon' => '<i class="fa fa-book"></i>',
                        'options' => ['class' => 'treeview',],
                        'active' => in_array(\Yii::$app->controller->id, ['order', 'status', 'payment', 'shipment',]),
                        'items' => [
                            ['label' => Yii::t('backend', 'Orders'), 'url' => ['/checkout/order/index',], 'icon' => '<i class="fa fa-shopping-bag"></i>', 'active' => 'order' === \Yii::$app->controller->id,],
                            ['label' => Yii::t('backend', 'Order status'), 'url' => ['/checkout/status/index',], 'icon' => '<i class="fa fa-shopping-basket"></i>', 'active' => 'status' === \Yii::$app->controller->id,],
                            ['label' => Yii::t('backend', 'Order payments'), 'url' => ['/checkout/payment/index',], 'icon' => '<i class="fa fa-envelope"></i>', 'active' => 'payment' === \Yii::$app->controller->id,],
                            ['label' => Yii::t('backend', 'Order shipments'), 'url' => ['/checkout/shipment/index',], 'icon' => '<i class="fa fa-ship"></i>', 'active' => 'shipment' === \Yii::$app->controller->id,],
                        ],
                    ],
                    [
                        'label' => Yii::t('backend', 'Promotions'),
                        'url' => '#',
                        'icon' => '<i class="fa fa-book"></i>',
                        'options' => ['class' => 'treeview',],
                        'active' => in_array(Yii::$app->controller->id, ['promotion', 'promotion-cart',]),
                        'items' => [
                            ['label' => Yii::t('backend', 'Promotions'), 'url' => ['/catalog/promotion/index',], 'icon' => '<i class="fa fa-shopping-bag"></i>', 'active' => 'promotion' === \Yii::$app->controller->id,],
                            ['label' => Yii::t('backend', 'Promotions cart'), 'url' => ['/catalog/promotion-cart/index',], 'icon' => '<i class="fa fa-shopping-cart"></i>', 'active' => 'promotion-cart' === \Yii::$app->controller->id,],
                        ],
                    ],
                    [
                        'label' => Yii::t('backend', 'Reports'),
                        'url' => ['/report/default/index',],
                        'icon' => '<i class="fa fa-bar-chart-o"></i>',
                        'options' => [],
                        'active' => Yii::$app->controller->module->id === 'report',
                    ],
                    [
                        'label' => Yii::t('backend', 'System'),
                        'options' => ['class' => 'header',],
                    ],
                    [
                        'label' => Yii::t('backend', 'Urls'),
                        'url' => '#',
                        'icon' => '<i class="fa fa-th-large"></i>',
                        'options' => ['class' => 'treeview',],
                        'active' => in_array(Yii::$app->controller->module->id, ['redirects', 'url',]),
                        'items' => [
                            [
                                'label' => Yii::t('backend', 'Semantic Urls'),
                                'url' => ['/url/semantic-url/index',],
                                'icon' => '<i class="fa fa-circle-o"></i>',
                                'active' => Yii::$app->controller->id === 'semantic-url',
                            ],
                            [
                                'label' => Yii::t('backend', 'Redirects'),
                                'url' => ['/redirects/redirects/index',],
                                'icon' => '<i class="fa fa-circle-o"></i>',
                                'active' => Yii::$app->controller->id === 'redirects',
                            ],
                        ],
                    ],
                    [
                        'label' => Yii::t('backend', 'Files'),
                        'url' => '#',
                        'icon' => '<i class="fa fa-th-large"></i>',
                        'options' => ['class' => 'treeview',],
                        'active' => Yii::$app->controller->module->id === 'file',
                        'items' => [
                            [
                                'label' => Yii::t('backend', 'Storage'),
                                'url' => ['/file/storage/index',],
                                'icon' => '<i class="fa fa-database"></i>',
                                'active' => Yii::$app->controller->id === 'storage',
                            ],
                            [
                                'label' => Yii::t('backend', 'Manager'),
                                'url' => ['/file/manager/index',],
                                'icon' => '<i class="fa fa-television"></i>',
                                'active' => Yii::$app->controller->id === 'manager',
                            ],
                        ],
                    ],
                    [
                        'label' => Yii::t('backend', 'Administration'),
                        'url' => '#',
                        'icon' => '<i class="fa fa-th-large"></i>',
                        'options' => ['class' => 'treeview',],
                        'active' => in_array(Yii::$app->controller->module->id, ['rbac', 'system', 'db-manager', 'translation',]),
                        'items' => [
                            [
                                'label' => Yii::t('backend', 'I18n'),
                                'url' => '#',
                                'icon' => '<i class="fa fa-language"></i>',
                                'options' => ['class' => 'treeview',],
                                'active' => Yii::$app->controller->module->id === 'translation',
                                'items' => [
                                    [
                                        'label' => Yii::t('backend', 'Translation'),
                                        'url' => ['/translation/default/index',],
                                        'icon' => '<i class="fa fa-language"></i>',
                                        'active' => Yii::$app->controller->id === 'default',
                                    ],
                                    [
                                        'label' => Yii::t('backend', 'Languages'),
                                        'url' => ['/translation/language/index',],
                                        'icon' => '<i class="fa fa-language"></i>',
                                        'visible' => Yii::$app->user->can('administrator'),
                                        'active' => Yii::$app->controller->id === 'language',
                                    ],
                                ],
                            ],
                            [
                                'label' => Yii::t('backend', 'Logs'),
                                'url' => '#',
                                'icon' => '<i class="fa fa-warning"></i>',
                                'options' => ['class' => 'treeview',],
                                'active' => in_array(\Yii::$app->controller->id, ['system-log', 'rest-api-log',]),
                                'items' => [
                                    [
                                        'label' => Yii::t('backend', 'Logs'),
                                        'url' => ['/system/system-log/index'],
                                        'icon' => '<i class="fa fa-warning"></i>',
                                        'badge' => SystemLog::find()->count(),
                                        'badgeBgClass' => 'label-danger',
                                        'active' => 'system-log' === \Yii::$app->controller->id,
                                    ],
                                    [
                                        'label' => Yii::t('backend', 'Rest API'),
                                        'url' => ['/system/rest-api-log/index'],
                                        'icon' => '<i class="fa fa-warning"></i>',
                                        'badge' => \backend\modules\system\models\RestLog::find()->count(),
                                        'badgeBgClass' => 'label-warning',
                                        'active' => 'rest-api-log' === \Yii::$app->controller->id,
                                    ],
                                ],
                            ],
                            [
                                'label' => Yii::t('backend', 'Cache'),
                                'url' => ['/system/cache/index'],
                                'icon' => '<i class="fa fa-refresh"></i>',
                                'active' => Yii::$app->controller->id === 'cache',
                            ],
                            [
                                'label' => Yii::t('backend', 'Key-Value Storage'),
                                'url' => ['/system/key-storage/index'],
                                'icon' => '<i class="fa fa-arrows-h"></i>',
                                'active' => Yii::$app->controller->id === 'key-storage',
                            ],
                            [
                                'label' => Yii::t('backend', 'RBAC Rules'),
                                'url' => '#',
                                'icon' => '<i class="fa fa-flag"></i>',
                                'options' => ['class' => 'treeview',],
                                'active' => Yii::$app->controller->module->id === 'rbac',
                                'visible' => Yii::$app->user->can('administrator'),
                                'items' => [
                                    [
                                        'label' => Yii::t('backend', 'Auth Assignment'),
                                        'url' => ['/rbac/rbac-auth-assignment/index'],
                                        'icon' => '<i class="fa fa-circle-o"></i>',
                                        'active' => Yii::$app->controller->id === 'rbac-auth-assignment',
                                    ],
                                    [
                                        'label' => Yii::t('backend', 'Auth Items'),
                                        'url' => ['/rbac/rbac-auth-item/index'],
                                        'icon' => '<i class="fa fa-circle-o"></i>',
                                        'active' => Yii::$app->controller->id === 'rbac-auth-item',
                                    ],
                                    [
                                        'label' => Yii::t('backend', 'Auth Item Child'),
                                        'url' => ['/rbac/rbac-auth-item-child/index'],
                                        'icon' => '<i class="fa fa-circle-o"></i>',
                                        'active' => Yii::$app->controller->id === 'rbac-auth-item-child',
                                    ],
                                    [
                                        'label' => Yii::t('backend', 'Auth Rules'),
                                        'url' => ['/rbac/rbac-auth-rule/index'],
                                        'icon' => '<i class="fa fa-circle-o"></i>',
                                        'active' => Yii::$app->controller->id === 'rbac-auth-rule',
                                    ],
                                ],
                            ],
                            [
                                'label' => Yii::t('backend', 'Db Manager'),
                                'url' => ['/db-manager'],
                                'icon' => '<i class="fa fa-database"></i>',
                                'active' => 'db-manager' === \Yii::$app->controller->module->id,
                            ],
                            [
                                'label' => Yii::t('backend', 'System Information'),
                                'url' => ['/system/information/index',],
                                'icon' => '<i class="fa fa-dashboard"></i>',
                                'active' => Yii::$app->controller->id === 'information',
                            ],
                        ],
                    ],
                ],
            ]) ?>
        </section>
        <!-- /.sidebar -->
    </aside>
    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?= $this->title ?>
                <?php if (isset($this->params['subtitle'])): ?>
                    <small><?= $this->params['subtitle'] ?></small>
                <?php endif; ?>
            </h1>
            <?php if (isset($this->params['returnUrl'])): ?>
                <?= yii\bootstrap\BaseHtml::a('<span class="glyphicon glyphicon-triangle-left"></span>' . Yii::t('backend', 'Return'), $this->params['returnUrl']); ?>
            <?php endif; ?>
            <?php if (isset($this->params['frontendUrl'])): ?>
                <?= yii\bootstrap\BaseHtml::a('<span class="glyphicon glyphicon-eye-open"></span> ' . Yii::t('backend', 'View on frontend'), $this->params['frontendUrl'], ['target' => '_blank',]); ?>
            <?php endif; ?>
            <?= Breadcrumbs::widget([
                'tag' => 'ol',
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php if (Yii::$app->session->hasFlash('alert')): ?>
                <?= Alert::widget([
                    'body' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                    'options' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
                ]) ?>
            <?php endif; ?>
            <?= $content ?>
        </section><!-- /.content -->
    </aside><!-- /.right-side -->
</div><!-- ./wrapper -->
<?php $this->endContent(); ?>

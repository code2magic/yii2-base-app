<?php
/**
 * @var yii\web\View $this
 * @var string $content
 */
?>
<?php $this->beginContent('@backend/views/layouts/common.php'); ?>
    <?= $content ?>
<?php $this->endContent(); ?>

<?php
/**
 * @var $this  yii\web\View
 * @var $model \common\models\Callback
 * @var $form  yii\bootstrap\ActiveForm
 * */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="article-form">
    <?php
    $form = ActiveForm::begin();
    ?>
    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::class) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'comment')->textarea() ?>
    <?= $form->field($model, 'is_processed')->widget(\kartik\widgets\SwitchInput::class, [
        'type' => \kartik\widgets\SwitchInput::CHECKBOX
    ]); ?>
    <div class="form-group field-album-created_at">
        <?= $form->field($model, 'form_name')->textInput(['maxlength' => true]) ?>
        <div class="help-block"></div>
    </div>
    <?php if (!$model->isNewRecord): ?>
        <div class="form-group field-album-created_at">
            <label class="control-label" for="album-name"><?= $model->getAttributeLabel('created_at'); ?></label>
            <br/>
            <?= Yii::$app->formatter->asDatetime($model->created_at) ?>
            <div class="help-block"></div>
        </div>
        <div class="form-group field-album-created_at">
            <label class="control-label" for="album-name"><?= $model->getAttributeLabel('updated_at'); ?></label>
            <br/>
            <?= Yii::$app->formatter->asDatetime($model->updated_at) ?>
            <div class="help-block"></div>
        </div>
    <?php endif; ?>
    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

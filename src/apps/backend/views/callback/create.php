<?php
/**
 * @var $this yii\web\View
 * @var $model \common\models\Callback
 */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => Yii::t('backend', 'Callback')
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Callbacks'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-create">
    <?= $this->render('_form', [
        'model' => $model
    ]) ?>
</div>

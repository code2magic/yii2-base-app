<?php
/**
 * @var $this yii\web\View
 * @var $model \common\models\Callback
 */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
        'modelClass' => Yii::t('backend', 'Callback')
    ]) . ' ' . Yii::$app->formatter->asDatetime($model->created_at);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Callbacks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="article-update">
    <?= $this->render('_form', [
        'model' => $model
    ]) ?>
</div>

<?php
/**
 * @var $this yii\web\View
 * @var $searchModel \backend\models\search\CallbackSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

use kartik\grid\EditableColumn;

$columns = [
    [
        'attribute' => 'id',
        'headerOptions' => ['class' => 'id-column',],
    ],
    'name',
    'phone',
    'email',
    'form_name',
    [
        'class' => EditableColumn::class,
        'attribute' => 'is_processed',
        'filter' => [
            Yii::t('backend', 'Not Processed'),
            Yii::t('backend', 'Processed'),
        ],
        'vAlign' => 'middle',
        'contentOptions' => ['style' => 'max-width: 50px;'],
        'readonly' => function ($model, $key, $index, $widget) {
            return (!(Yii::$app->user->can('manager') || Yii::$app->user->can('administrator'))); // do not allow editing of inactive records
        },
        'editableOptions' => [
            'format' => \kartik\editable\Editable::FORMAT_BUTTON,
            'displayValueConfig' => ['0' => \kartik\grid\GridView::ICON_INACTIVE, '1' => \kartik\grid\GridView::ICON_ACTIVE,],
            'inputType' => \kartik\editable\Editable::INPUT_SWITCH,
            'placement' => 'auto',
        ],
    ],
    'created_at:datetime',
    'updated_at:datetime',
];
echo $this->render('@core/views/_common/index', [
    'columns' => $columns,
    'title' => Yii::t('backend', 'Callbacks'),
    'title_create' => Yii::t('backend', 'Callback'),
    'dataProvider' => $dataProvider,
    'searchModel' => $searchModel,
]);

<?php
/**
 * @var yii\web\View $this
 * @var yii\bootstrap\ActiveForm $form
 * @var code2magic\baseApp\backend\models\UserForm $model
 * @var array $roles
 */
?>
<?= $form->field($model, 'roles')->checkboxList($roles) ?>

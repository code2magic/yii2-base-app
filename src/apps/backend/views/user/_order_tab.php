<?php
/**
 * @var \yii\web\View $this
 * @var \backend\models\UserForm $model
 * @var \code2magic\checkout\backend\models\Order[] $orders
 * @var \common\models\UserWishlist[] $wishlists
 * @var \code2magic\checkout\frontend\models\CartPosition[] $cart_positions
 */

use yii\helpers\Url;

?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'User Orders') ?></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="">
        <div class="table-responsive">
            <table class="table table-striped no-margin">
                <thead>
                <tr>
                    <th><?= Yii::t('backend', 'Order Id') ?></th>
                    <th><?= Yii::t('backend', 'Status') ?></th>
                    <th><?= Yii::t('backend', 'Total') ?></th>
                    <th><?= Yii::t('backend', 'Craeted at') ?></th>
                    <th><?= Yii::t('backend', 'Finished at') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($orders as $order): ?>
                    <tr>
                        <td>
                            <a href="<?= Url::toRoute(['/checkout/order/update', 'id' => $order->id]) ?>">
                                <?= $order->id ?>
                            </a>
                        </td>
                        <td><span class="label label-info"><?= $order->orderStatus->name ?></span></td>
                        <td><?= $order->getTotal_cost() ?></td>
                        <td><?= $order->created_at ?></td>
                        <td><?= $order->updated_at ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
    <!-- /.box-footer -->
</div>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'User Shopping cart') ?></h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>

        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="">
        <div class="table-responsive">
            <table class="table table-striped no-margin">
                <thead>
                <tr>
                    <th><?= Yii::t('backend', 'Product') ?></th>
                    <th><?= Yii::t('backend', 'Quantity') ?></th>
                    <th><?= Yii::t('backend', 'Base price') ?></th>
                    <th><?= Yii::t('backend', 'Price') ?></th>
                    <th><?= Yii::t('backend', 'Total cost') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($cart_positions as $position): ?>
                    <tr>
                        <td>
                            <a href="<?= Url::toRoute(['/catalog/product/update', 'id' => $position->getProductId()]) ?>">
                                <?= $position->getName() ?>
                            </a>
                        </td>

                        <td><?= $position->quantity ?></td>
                        <td><?= $position->getPrice(false) ?></td>
                        <td><?= $position->getPrice() ?></td>
                        <td><?= $position->getCost() ?></td>

                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
    <!-- /.box-footer -->
</div>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'User Wishlist') ?></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="">
        <div class="table-responsive">
            <table class="table table-striped no-margin">
                <thead>
                <tr>
                    <th><?= Yii::t('backend', 'Product') ?></th>
                    <th><?= Yii::t('backend', 'Created at') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($wishlists as $wishlist): ?>
                    <tr>
                        <td>
                            <a href="<?= Url::toRoute([
                                '/catalog/product/update', 'id' => $wishlist->catalog_product_id
                            ]) ?>">
                                <?= \yii\helpers\ArrayHelper::getValue($wishlist, ['product', 'name',], '-') ?>
                            </a>
                        </td>
                        <td><?= Yii::$app->formatter->asDatetime($wishlist->created_at) ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
    <!-- /.box-footer -->
</div>

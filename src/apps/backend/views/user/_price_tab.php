<?php
/**
 * @var \yii\web\View $this
 * @var yii\bootstrap\ActiveForm $form
 * @var \backend\models\UserForm $model
 */
?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Catalog data') ?></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body" style="">
        <?= $form->field($model, 'catalog_user_customer_group_id')->widget(
            \kartik\widgets\Select2::class,
            [
                'data' => \code2magic\catalog\backend\models\UserCustomerGroup::getForSelect($model->catalog_user_customer_group_id),
                'pluginOptions' => [
                    'allowClear' => true,
                    'ajax' => [
                        'url' => \yii\helpers\Url::toRoute(['/catalog/user-customer-group/select2',]),
                        'delay' => 500,
                    ],
                ],
            ]
        );?>
    </div>
</div>

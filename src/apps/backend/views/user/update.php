<?php
/**
 * @var yii\web\View $this
 * @var code2magic\baseApp\models\User $model
 * @var yii\rbac\Role[] $roles
 */

$this->title = Yii::t('backend', 'Update {modelClass}: ', ['modelClass' => 'User']) . ' ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->email, 'url' => ['view', 'id' => $model->email]];
$this->params['breadcrumbs'][] = ['label'=>Yii::t('backend', 'Update')];
?>
<div class="user-update">
    <?= $this->render('_form', compact('model',
        'roles',
        'orders',
        'wishlists',
        'cart_positions',
        'availablePromotionCart',
        'usedPromotionCart'
    )) ?>
</div>

<?php
/**
 * @var $this yii\web\View
 * @var $model code2magic\baseApp\backend\models\UserForm
 * @var $roles yii\rbac\Role[]
 */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'User',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">
    <?= $this->render('_form', compact(
        'model',
        'roles',
        'orders',
        'wishlists',
        'cart_positions',
        'availablePromotionCart',
        'usedPromotionCart'
    )) ?>
</div>

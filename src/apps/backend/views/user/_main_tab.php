<?php
/**
 * @var \yii\web\View $this
 * @var yii\bootstrap\ActiveForm $form
 * @var \backend\models\UserForm $model
 */
?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Main data') ?></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body" style="">
        <?= $form->field($model, 'username', ['options' => ['autocomplete' => 'false',],]) ?>
        <?= $form->field($model, 'email', ['options' => ['autocomplete' => 'false',],]) ?>
        <?= $form->field($model, 'password', ['options' => ['autocomplete' => 'false',],])
            ->passwordInput() ?>
        <?= $form->field($model, 'password_confirm', ['options' => ['autocomplete' => 'false',],])
            ->passwordInput() ?>
        <?= $form->field($model, 'status')
            ->dropDownList(\common\models\User::statuses()) ?>
    </div>
</div>

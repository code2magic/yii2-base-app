<?php
/**
 * @var $this yii\web\View
 * @var $model code2magic\baseApp\backend\models\UserForm
 * @var $form yii\bootstrap\ActiveForm
 * @var $roles yii\rbac\Role[]
 * @var $permissions yii\rbac\Permission[]
 */

use kartik\tabs\TabsX;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="user-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model); ?>
    <?php
    $tabs = [
        [
            'label' => Yii::t('backend', 'Main'),
            'content' => $this->render('_main_tab', compact('form', 'model')),
        ],
        [
            'label' => Yii::t('backend', 'User Profile'),
            'content' => $this->render('_profile_tab', compact('form', 'model')),
        ],
        [
            'label' => Yii::t('backend', 'Prices'),
            'content' => $this->render('_price_tab', compact('form', 'model')),
        ],
    ];
    if (!$model->getModel()->getIsNewRecord()) {
        $tabs[] = [
            'label' => Yii::t('backend', 'Orders and wishlists'),
            'content' => $this->render(
                '_order_tab',
                compact(
                    'form',
                    'model',
                    'orders',
                    'wishlists',
                    'cart_positions'
                )
            ),
            'visible' => !$model->getModel()->getIsNewRecord(),
        ];
    }
    if (Yii::$app->user->can('administrator')) {
        $tabs[] = [
            'label' => Yii::t('backend', 'Roles'),
            'content' => $this->render('_roles_tab', compact('form', 'model', 'roles')),
        ];
    }
    if (!$model->getModel()->getIsNewRecord()) {
        $tabs[] = [
            'label' => Yii::t('backend', 'Promotion Cart'),
            'content' => $this->render('_promotion_cart_tab', compact('form', 'model', 'availablePromotionCart', 'usedPromotionCart')),
            'visible' => Yii::$app->user->can('administrator'),
        ];
    }
    echo TabsX::widget([
        'items' => $tabs,
        'enableStickyTabs' => true,
        'stickyTabsOptions' => [
            'selectorAttribute' => 'data-target',
            'backToTop' => false,
        ],
        'position' => TabsX::POS_ABOVE,
        'id' => 'main_tabs',
        'encodeLabels' => false,
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-primary', 'name' => 'signup-button',]) ?>
        <?= Html::submitButton(Yii::t('backend', 'Save and stay'), ['class' => 'btn btn-success', 'name' => 'stay',]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

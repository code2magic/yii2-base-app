<?php
/**
 * @var yii\web\View $this
 * @var code2magic\baseApp\backend\models\search\UserSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

use code2magic\core\grid\column\Enum;
use code2magic\baseApp\models\User;
use trntv\yii\datetime\DateTimeWidget;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\JsExpression;

$this->title = Yii::t('backend', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@backend/views/_common/box-wrap.php'); ?>
<div class="user-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a(Yii::t('backend', 'Create {modelClass}', [
            'modelClass' => Yii::t('backend', 'User'),
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'grid-view table-responsive'
        ],
        'columns' => [
            //[
            //    'format' => 'raw',
            //    'label' => Yii::t('backend', 'Avatar'),
            //    'value' => function ($model, $key, $index, $column) {
            //        return Html::img($model->userProfile->getAvatar('/img/anonymous.jpg'), ['style' => 'max-width: 100px;max-height: 100px;']);
            //    },
            //],
            //'id',
            [
                'format' => 'raw',
                'attribute' => 'fullname',
                'label' => $searchModel->getAttributeLabel('fullname'),
                'value' => function ($model, $key, $index, $column) {
                    /** @var User $model */
                    return Html::a($model->publicIdentity, ['user/update', 'id' => $model->id,]);
                },
            ],
            'email:email',
            [
                'attribute' => 'phone',
                'value' => 'userProfile.phone',
                'label' => $searchModel->getAttributeLabel('phone'),
            ],
            [
                'attribute' => 'user_customer_group',
                'value' => 'userProfile.userCustomerGroupI18n.name',
                'filter' => \yii\helpers\ArrayHelper::map(
                    \code2magic\catalog\backend\models\UserCustomerGroup::find()->localized()->all(),
                    'id',
                    'name'
                ),
                'label' => $searchModel->getAttributeLabel('user_customer_group'),
            ],
            [
                'attribute' => 'region',
                'value' => 'userProfile.city.region.name',
                'label' => $searchModel->getAttributeLabel('region'),
            ],
            [
                'attribute' => 'city',
                'value' => 'userProfile.city.name',
                'label' => $searchModel->getAttributeLabel('city'),
            ],
            [
                'class' => Enum::class,
                'attribute' => 'status',
                'enum' => User::statuses(),
                'filter' => User::statuses(),
            ],
            [
                'attribute' => 'created_at',
                'format' => 'datetime',
                'filter' => DateTimeWidget::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'phpDatetimeFormat' => 'dd.MM.yyyy',
                    'momentDatetimeFormat' => 'DD.MM.YYYY',
                    'clientEvents' => [
                        'dp.change' => new JsExpression('(e) => $(e.target).find("input").trigger("change.yiiGridView")'),
                    ],
                ])
            ],
            [
                'attribute' => 'logged_at',
                'format' => 'datetime',
                'filter' => DateTimeWidget::widget([
                    'model' => $searchModel,
                    'attribute' => 'logged_at',
                    'phpDatetimeFormat' => 'dd.MM.yyyy',
                    'momentDatetimeFormat' => 'DD.MM.YYYY',
                    'clientEvents' => [
                        'dp.change' => new JsExpression('(e) => $(e.target).find("input").trigger("change.yiiGridView")'),
                    ],
                ])
            ],
            // 'updated_at',
            [
                'class' => yii\grid\ActionColumn::class,
                //'template' => '{login} {view} {update} {delete}',
                'buttons' => [
                    'login' => function ($url) {
                        return Html::a(
                            '<i class="fa fa-sign-in" aria-hidden="true"></i>',
                            $url,
                            ['title' => Yii::t('backend', 'Login'),]
                        );
                    },
                ],
                'visibleButtons' => [
                    'login' => Yii::$app->user->can('administrator'),
                    'delete' => Yii::$app->user->can('administrator'),
                ],
            ],
        ],
    ]); ?>
</div>
<?php $this->endContent(); ?>

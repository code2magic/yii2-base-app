<?php
/**
 * @var \yii\web\View $this
 * @var yii\bootstrap\ActiveForm $form
 * @var \backend\models\UserForm $model
 * @var \code2magic\catalog\backend\models\PromotionCart[] $availablePromotionCart
 * @var \code2magic\catalog\backend\models\PromotionCart[] $usedPromotionCart
 */

use yii\helpers\Url;

?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('backend', 'Available Promotion Cart') ?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body" style="">
                <div class="table-responsive">
                    <table class="table table-striped no-margin">
                        <thead>
                        <tr>
                            <th><?= Yii::t('backend', 'Promotion Cart Id') ?></th>
                            <th><?= Yii::t('backend', 'Name') ?></th>
                            <th><?= Yii::t('backend', 'Code') ?></th>
                            <th><?= Yii::t('backend', 'Value') ?></th>
                            <th><?= Yii::t('backend', 'Beginning date') ?></th>
                            <th><?= Yii::t('backend', 'Ending date') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($availablePromotionCart as $item): ?>
                            <tr>
                                <td>
                                    <a href="<?= Url::toRoute(['/catalog/promotion-cart/update', 'id' => $item->id]) ?>">
                                        <?= $item->id ?>
                                    </a>
                                </td>
                                <td><span class="label label-info"><?= $item->name ?></span></td>
                                <td><span class="label label-info"><?= $item->code ?></span></td>
                                <td><span class="label label-info"><?= $item->value ?></span></td>
                                <td><?= $item->date_begin ?></td>
                                <td><?= $item->date_end ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                <div class="box-footer">
                    <a href="<?= Url::toRoute(['/catalog/promotion-cart/create', 'user_id' => $model->id]) ?>"
                       class="btn btn-success pull-right"
                    >
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-info ">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('backend', 'Used Promotion Cart') ?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body" style="">
                <div class="table-responsive">
                    <table class="table table-striped no-margin">
                        <thead>
                        <tr>
                            <th><?= Yii::t('backend', 'Promotion Cart Id') ?></th>
                            <th><?= Yii::t('backend', 'Order Id') ?></th>
                            <th><?= Yii::t('backend', 'Name') ?></th>
                            <th><?= Yii::t('backend', 'Code') ?></th>
                            <th><?= Yii::t('backend', 'Value') ?></th>
                            <th><?= Yii::t('backend', 'Beginning date') ?></th>
                            <th><?= Yii::t('backend', 'Ending date') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($usedPromotionCart as $item): ?>
                            <tr>
                                <td>
                                    <a href="<?= Url::toRoute(['/catalog/promotion-cart/update', 'id' => $item->id]) ?>">
                                        <?= $item->id ?>
                                    </a>
                                </td>
                                <td>
                                    <a href="<?= Url::toRoute(['/checkout/order/update', 'id' => $item->order->id]) ?>">
                                        <?= $item->order->id ?>
                                    </a>
                                </td>
                                <td><span class="label label-info"><?= $item->name ?></span></td>
                                <td><span class="label label-info"><?= $item->code ?></span></td>
                                <td><span class="label label-info"><?= $item->value ?></span></td>
                                <td><?= $item->date_begin ?></td>
                                <td><?= $item->date_end ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
        </div>
    </div>
</div>

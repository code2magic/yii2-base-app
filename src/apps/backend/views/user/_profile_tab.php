<?php
/**
 * @var \yii\web\View $this
 * @var yii\bootstrap\ActiveForm $form
 * @var \backend\models\UserForm $model
 */

use code2magic\baseApp\assets\NestedSelect2Asset;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

NestedSelect2Asset::register($this);
\code2magic\geo\assets\CitySelectorAsset::register($this);
?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'User data') ?></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body" style="">
        <?= $form->field($model, 'firstname')->textInput(['maxlength' => true,]) ?>
        <?= $form->field($model, 'middlename')->textInput(['maxlength' => true,]) ?>
        <?= $form->field($model, 'lastname')->textInput(['maxlength' => true,]) ?>
        <?= $form->field($model, 'phone') ?>
        <?= $form->field($model, 'is_subscribed')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'birthday')->widget(\kartik\widgets\DateTimePicker::class) ?>
        <?= $form->field($model, 'comment')->textarea() ?>
    </div>
</div>
<div class="box box-info ">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('backend', 'Address') ?></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body" style="">
        <div class="form-group row field-userform-geo_city_id">
            <div class="col-md-12">
                <?= Html::activeLabel($model, 'geo_city_id') ?>
                <div class="row city-selector">
                    <div class="col-md-12 col-lg-4">
                        <?= \kartik\select2\Select2::widget([
                            'name' => 'country_id',
                            'value' => ArrayHelper::getValue($model, ['city', 'region', 'geo_country_id',]),
                            'data' => \code2magic\geo\helpers\Geo::getCountries(
                                ArrayHelper::getValue($model, ['city', 'region', 'geo_country_id',])
                            ),
                            'options' => [
                                'id' => 'country',
                                'placeholder' => '',
                                'prompt' => '-',
                                'data' => [
                                    'child_element' => '#region',
                                ],
                            ],
                            'pluginOptions' => [
                                'ajax' => [
                                    'url' => \yii\di\Instance::ensure('urlManager-api')
                                        ->createAbsoluteUrl(['/geo/country/select2',]),
                                    'delay' => 500,
                                ],
                            ],
                        ]) ?>
                    </div>
                    <div class="col-md-12 col-lg-4">
                        <?= \kartik\select2\Select2::widget([
                            'name' => 'geo_region_id',
                            'value' => ArrayHelper::getValue($model, ['city', 'geo_region_id',]),
                            'data' => \code2magic\geo\helpers\Geo::getRegions(
                                ArrayHelper::getValue($model, ['city', 'region', 'geo_country_id',]),
                                ArrayHelper::getValue($model, ['city', 'geo_region_id',])
                            ),
                            'options' => [
                                'id' => 'region',
                                'placeholder' => '',
                                'prompt' => '-',
                                'data' => [
                                    'parent_element' => '#country',
                                    'parent_param' => 'country_id',
                                    'child_element' => '#city',
                                ],
                            ],
                            'pluginOptions' => [
                                'ajax' => [
                                    'url' => \yii\di\Instance::ensure('urlManager-api')
                                        ->createAbsoluteUrl(['/geo/region/select2',]),
                                    'data' => NestedSelect2Asset::getSelect2DataFunction(),
                                    'delay' => 500,
                                ],
                            ],
                        ]) ?>
                    </div>
                    <div class="col-md-12 col-lg-4">
                        <?= \kartik\select2\Select2::widget(['model' => $model,
                            'attribute' => 'geo_city_id',
                            'data' => \code2magic\geo\helpers\Geo::getCities(
                                ArrayHelper::getValue($model, ['city', 'geo_region_id',]),
                                ArrayHelper::getValue($model, ['geo_city_id',])
                            ),
                            'options' => [
                                'id' => 'city',
                                'placeholder' => '',
                                'prompt' => '-',
                                'data' => [
                                    'parent_element' => '#region',
                                    'parent_param' => 'region_id',
                                ],
                            ],
                            'pluginOptions' => [
                                'ajax' => [
                                    'url' => \yii\di\Instance::ensure('urlManager-api')
                                        ->createAbsoluteUrl(['/geo/city/select2',]),
                                    'data' => NestedSelect2Asset::getSelect2DataFunction(),
                                    'delay' => 500,
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
                <?= Html::error($model, 'geo_city_id') ?>
            </div>
        </div>
        <?= $form->field($model, 'address_region')->textInput(['maxlength' => true,]) ?>
        <?= $form->field($model, 'address_city')->textInput(['maxlength' => true,]) ?>
        <?= $form->field($model, 'address_street')->textInput(['maxlength' => true,]) ?>
        <?= $form->field($model, 'address_building')->textInput(['maxlength' => true,]) ?>
        <?= $form->field($model, 'address_apt')->textInput(['maxlength' => true,]) ?>
    </div>
</div>

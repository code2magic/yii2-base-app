<?php

use code2magic\baseApp\models\UserProfile;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model code2magic\baseApp\models\UserProfile
 * @var $form yii\bootstrap\ActiveForm
 */

$this->title = Yii::t('backend', 'Edit profile')
?>
<div class="user-profile-form">
    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'picture')->widget(\trntv\filekit\widget\Upload::class, [
        'url'=>['avatar-upload']
    ]) ?>
    <?= $form->field($model, 'firstname')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'middlename')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'lastname')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'locale')->dropDownlist(\code2magic\i18n\helpers\Locale::getLanguages()) ?>
    <?= $form->field($model, 'gender')->dropDownlist([
        UserProfile::GENDER_FEMALE => Yii::t('backend', 'Female'),
        UserProfile::GENDER_MALE => Yii::t('backend', 'Male')
    ]) ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Update'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>

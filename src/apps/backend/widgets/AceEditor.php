<?php

namespace code2magic\baseApp\backend\widgets;

use trntv\aceeditor\AceEditorAsset;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\widgets\InputWidget;

/**
 * Class AceEditor
 * @package trntv\aceeditor
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class AceEditor extends InputWidget
{
    /**
     * @var boolean Read-only mode on/off (false=off - default)
     */
    public $readOnly = false;

    /**
     * @var string Programming Language Mode
     */
    public $mode = 'html';

    /**
     * @var string Editor theme
     * $see Themes List
     * @link https://github.com/ajaxorg/ace/tree/master/lib/ace/theme
     */
    public $theme = 'tomorrow_night';

    /**
     * @var array Div options
     */
    public $containerOptions = [
        'style' => 'width: 100%; min-height: 400px'
    ];

    public $aceOptions = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        AceEditorAsset::register($this->getView());
        $editor_id = str_replace('-', '_', $this->getId());
        $editor_var = 'aceeditor_' . $editor_id;
        $this->getView()->registerJs("var {$editor_var} = ace.edit(\"{$editor_id}\")");
        $this->getView()->registerJs("{$editor_var}.setTheme(\"ace/theme/{$this->theme}\")");
        $this->getView()->registerJs("{$editor_var}.getSession().setMode(\"ace/mode/{$this->mode}\")");
        $this->getView()->registerJs("{$editor_var}.setReadOnly({$this->readOnly})");
        $this->getView()->registerJs("{$editor_var}.setOptions(" . Json::encode($this->aceOptions) . ")");
        $this->getView()->registerJs(<<<JS
ace.config.loadModule("ace/ext/language_tools", function() {
    ace.require("ace/ext/language_tools"),
    {$editor_var}.setOptions({
        enableBasicAutocompletion: !0,
        enableSnippets: !0,
        enableLiveAutocompletion: !0
    })
})
JS
        );

        $textarea_var = 'acetextarea_' . $editor_id;
        $this->getView()->registerJs("
            var {$textarea_var} = $('#{$this->options['id']}').hide();
            {$editor_var}.getSession().setValue({$textarea_var}.val());
            {$editor_var}.getSession().on('change', function(){
                {$textarea_var}.val({$editor_var}.getSession().getValue());
            });
        ");
        Html::addCssStyle($this->options, 'display: none');
        $this->containerOptions['id'] = $editor_id;
        $this->getView()->registerCss("#{$editor_id}{position:relative}");
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $content = Html::tag('div', '', $this->containerOptions);
        if ($this->hasModel()) {
            $content .= Html::activeTextarea($this->model, $this->attribute, $this->options);
        } else {
            $content .= Html::textarea($this->name, $this->value, $this->options);
        }
        return $content;
    }
}

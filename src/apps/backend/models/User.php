<?php
namespace code2magic\baseApp\backend\models;

/**
 * Class User
 *
 * @property string $backendName
 *
 * @package code2magic\baseApp\backend\models
 */
class User extends \common\models\User
{
    /**
     * @return string
     */
    public function getBackendName(): string
    {
        /** @var User $item */
        return "{$this->fullName} ({$this->username})";
    }
}
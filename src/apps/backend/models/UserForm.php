<?php

namespace code2magic\baseApp\backend\models;

use borales\extensions\phoneInput\PhoneInputValidator;
use code2magic\catalog\backend\models\UserCustomerGroup;
use code2magic\geo\backend\models\City;
use code2magic\baseApp\models\User;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\base\UnknownMethodException;
use yii\base\UnknownPropertyException;
use yii\helpers\ArrayHelper;

/**
 * Create user form
 */
class UserForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_confirm;
    public $status;
    public $roles;

    public $firstname;
    public $middlename;
    public $lastname;
    public $address_region;
    public $address_city;
    public $geo_city_id;
    public $address_street;
    public $address_building;
    public $address_apt;
    public $phone;
    public $catalog_user_customer_group_id;
    public $is_subscribed;
    public $comment;
    public $birthday;

    private $model;

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        try {
            return parent::__get($name);
        } catch (UnknownPropertyException $e) {
            return $this->model->$name;
        }
    }

    public function __set($name, $value)
    {
        try {
            return parent::__set($name, $value);
        } catch (UnknownPropertyException $e) {
            $this->model->$name = $value;
        }
    }

    public function __call($name, $params)
    {
        try {
            return parent::__call($name, $params);
        } catch (UnknownMethodException $e) {
            return \call_user_func([$this->model, $name], $params);
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catalog_user_customer_group_id'], 'integer'],
            [['catalog_user_customer_group_id'], 'default', 'value' => function ($model) {
                return ArrayHelper::getValue(UserCustomerGroup::getDefault(), 'id');
            }],
            [['username',], 'filter', 'filter' => 'trim'],
            [['username',], 'required'],
            [['username',], 'unique', 'targetClass' => User::class, 'filter' => function ($query) {
                if (!$this->getModel()->isNewRecord) {
                    $query->andWhere(['not', ['id' => $this->getModel()->id]]);
                }
            }],
            [['username',], 'string', 'min' => 2, 'max' => 255],
            [
                [
                    'firstname',
                    'lastname',
                    'middlename',
                    'address_region',
                    'address_city',
                    'address_apt',
                    'address_street',
                    'address_building',
                ],
                'string',
                'max' => 255,
            ],
            [
                [
                    'firstname',
                    'lastname',
                    'middlename',
                    'address_region',
                    'address_city',
                    'address_apt',
                    'address_street',
                    'address_building',
                    'phone',
                    'comment',
                ],
                'default',
                'value' => '',
            ],
            [['comment',], 'string',],
            [['birthday',], 'date',],
            [['geo_city_id',], 'exist', 'targetClass' => City::class, 'targetAttribute' => ['geo_city_id' => 'id']],
            [['is_subscribed',], 'boolean'],
            [['is_subscribed',], 'default', 'value' => 1],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::class, 'filter' => function ($query) {
                if (!$this->getModel()->isNewRecord) {
                    $query->andWhere(['not', ['id' => $this->getModel()->id]]);
                }
            }],

            ['password', 'required', 'on' => 'create'],
            ['password', 'string', 'min' => 6],
            [['password_confirm'], 'compare', 'compareAttribute' => 'password'],
            ['password_confirm', 'required', 'when' => function ($model) {
                return ($model->password) ? 1 : 0;
            }, 'whenClient' => "function(attribute, value){return $('#accountform-password').val()?1:0;}"],
            [['status'], 'integer'],
            [['phone'], 'string'],
            [['phone'], PhoneInputValidator::class],
            [['roles'], 'each', 'rule' => ['in', 'range' => function ($model, $attribute) {
                return ArrayHelper::getColumn(
                    Yii::$app->authManager->getRoles(),
                    'name'
                );
            }],
            ],
        ];
    }

    /**
     * @return User
     */
    public function getModel()
    {
        if (!$this->model) {
            $this->model = new User();
        }
        return $this->model;
    }

    /**
     * @param User $model
     *
     * @return mixed
     */
    public function setModel($model)
    {
        $this->load($model->attributes, '');
        $this->load($model->userProfile->attributes, '');
        $this->model = $model;
        $this->roles = ArrayHelper::getColumn(
            Yii::$app->authManager->getRolesByUser($model->getId()),
            'name'
        );
        return $this->model;
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return ArrayHelper::getValue($this->getModel(), ['userProfile', 'city']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('common', 'Username'),
            'email' => Yii::t('common', 'E-mail'),
            'status' => Yii::t('common', 'Status'),
            'password' => Yii::t('common', 'Password'),
            'roles' => Yii::t('common', 'Roles'),
            'birthday' => Yii::t('model_labels', 'Birthday'),
            'address_region' => Yii::t('model_labels', 'Address Region'),
            'address_building' => Yii::t('model_labels', 'Address Building'),
            'address_city' => Yii::t('model_labels', 'Address City'),
            'geo_city_id' => Yii::t('model_labels', 'City'),
            'address_street' => Yii::t('model_labels', 'Address Street'),
            'address_apt' => Yii::t('model_labels', 'Address Apt'),
            'firstname' => Yii::t('model_labels', 'Firstname'),
            'lastname' => Yii::t('model_labels', 'Lastname'),
            'middlename' => Yii::t('model_labels', 'Middlename'),
            'phone' => Yii::t('model_labels', 'Phone'),
            'comment' => Yii::t('model_labels', 'Comment'),
            'password_confirm' => Yii::t('backend', 'Password Confirm'),
            'catalog_user_customer_group_id' => Yii::t('model_labels', 'User customer group'),
            'is_subscribed' => Yii::t('frontend/user/model_labels', 'Is Subscribed'),

        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     * @throws Exception
     */
    public function save()
    {
        if ($this->validate()) {
            $model = $this->getModel();
            $isNewRecord = $model->getIsNewRecord();
            $model->username = $this->username;
            $model->email = $this->email;
            $model->status = $this->status;
            if ($this->password) {
                $model->setPassword($this->password);
            }
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if (!$model->save()) {
                    throw new Exception('Model not saved');
                }
                if ($isNewRecord) {
                    $model->afterSignup($this->attributes);
                } else {
                    $model->userProfile->load($this->attributes, '');
                    if(!array_key_exists($model->userProfile->locale, \code2magic\i18n\helpers\Locale::getLanguages())){
                        $model->userProfile->locale = null;
                    }
                    if (!$model->userProfile->save()) {
                        throw new Exception('Model not saved');
                    }
                }
                $auth = Yii::$app->authManager;
                $auth->revokeAll($model->getId());

                if ($this->roles && \is_array($this->roles)) {
                    foreach ($this->roles as $role) {
                        $auth->assign($auth->getRole($role), $model->getId());
                    }
                }
            } catch (\Exception $e) {
                $this->addErrors(['Exception' => $e->getMessage()]);
                $transaction->rollBack();
                return false;
            }
            $transaction->commit();
            return !$model->hasErrors();
        }
        return null;
    }
}

<?php

namespace code2magic\baseApp\backend\models;

use code2magic\geo\backend\models\City;

/**
 * Class UserProfile
 * @package code2magic\baseApp\backend\models
 */
class UserProfile extends \common\models\UserProfile
{
    //<editor-fold desc="Relations">

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        $query = $this->hasOne(City::class, ['geo_city_id' => 'id']);
        return $query;
    }

    //</editor-fold>
}
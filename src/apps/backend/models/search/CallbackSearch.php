<?php

namespace code2magic\baseApp\backend\models\search;

use code2magic\baseApp\models\Callback;
use yii\data\ActiveDataProvider;


/**
 * CallbackSearch represents the model behind the search form about `common\models\Callback`.
 */
class CallbackSearch extends Callback
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'is_processed',], 'integer',]
        ];
    }


    /**
     * Creates data provider instance with search query applied
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Callback::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC,],
            ],
        ]);
        if (isset($_GET['CallbackSearch']) && !($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'is_processed' => $this->is_processed,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
        return $dataProvider;
    }
}

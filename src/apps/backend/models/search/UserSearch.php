<?php

namespace code2magic\baseApp\backend\models\search;

use code2magic\baseApp\backend\models\User;
use code2magic\baseApp\backend\models\UserProfile;
use code2magic\catalog\models\UserCustomerGroup;
use code2magic\geo\models\CityI18n;
use code2magic\geo\models\RegionI18n;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * UserSearch represents the model behind the search form about `backend\models\User`.
 */
class UserSearch extends User
{
    public $phone;
    public $region;
    public $city;
    public $fullname;
    public $user_customer_group;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status',], 'integer'],
            [['created_at', 'updated_at', 'logged_at',], 'filter', 'filter' => 'strtotime', 'skipOnEmpty' => true,],
            [['created_at', 'updated_at', 'logged_at',], 'default', 'value' => null,],
            [['phone', 'user_customer_group', 'region', 'city', 'username', 'auth_key', 'password_hash', 'email', 'fullname',], 'safe',],
        ];
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'phone' => \Yii::t('model_labels', 'Phone'),
            'user_customer_group' => \Yii::t('model_labels', 'User customer group'),
            'city' => \Yii::t('model_labels', 'City'),
            'region' => \Yii::t('model_labels', 'Region'),
            'fullname' => \Yii::t('model_labels', 'Fullname'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();
        $query->joinWith(['userProfile.city' => function ($query) {
            $query->joinWith(['i18ns', 'region.i18ns',]);
        },]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC,],
            ],
        ]);
        $dataProvider->sort->attributes['fullname'] = [
            'asc' => [
                UserProfile::tableName() . '.firstname' => SORT_ASC,
                UserProfile::tableName() . '.middlename' => SORT_ASC,
                UserProfile::tableName() . '.lastname' => SORT_ASC,
            ],
            'desc' => [
                UserProfile::tableName() . '.firstname' => SORT_DESC,
                UserProfile::tableName() . '.middlename' => SORT_DESC,
                UserProfile::tableName() . '.lastname' => SORT_DESC,
            ],
        ];
        $dataProvider->sort->attributes['phone'] = [
            'asc' => [UserProfile::tableName() . '.phone' => SORT_ASC,],
            'desc' => [UserProfile::tableName() . '.phone' => SORT_DESC,],
        ];
        $dataProvider->sort->attributes['user_customer_group'] = [
            'asc' => [UserProfile::tableName() . '.catalog_user_customer_group_id' => SORT_ASC,],
            'desc' => [UserProfile::tableName() . '.catalog_user_customer_group_id' => SORT_DESC,],
        ];
        $dataProvider->sort->attributes['city'] = [
            'asc' => [CityI18n::tableName() . '.name' => SORT_ASC,],
            'desc' => [CityI18n::tableName() . '.name' => SORT_DESC,],
        ];
        $dataProvider->sort->attributes['region'] = [
            'asc' => [RegionI18n::tableName() . '.name' => SORT_ASC,],
            'desc' => [RegionI18n::tableName() . '.name' => SORT_DESC,],
        ];
        if (!\Yii::$app->user->can('administrator')) {
            $admin = \Yii::$app->authManager->getUserIdsByRole('administrator');
            $manager = \Yii::$app->authManager->getUserIdsByRole('manager');
            $not_shown = array_merge($admin, $manager);
            $query->andWhere(['not in', 'id', $not_shown,]);
        }
        if (!($this->load($params) && $this->validate())) {
            $query->distinct = false;
            $count = $query->count('DISTINCT ' . static::tableName() . '.id');
            $query->distinct = true;
            $dataProvider->totalCount = $count;
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id' => $this->id,
            User::tableName() . '.status' => $this->status,
        ]);
        $defaultGroup = UserCustomerGroup::getDefault();
        if ($this->created_at !== null) {
            $query->andFilterWhere(['between', 'created_at', $this->created_at, $this->created_at + 3600 * 24,]);
        }
        if ($this->updated_at !== null) {
            $query->andFilterWhere(['between', 'updated_at', $this->updated_at, $this->updated_at + 3600 * 24,]);
        }
        if ($this->logged_at !== null) {
            $query->andFilterWhere(['between', 'logged_at', $this->logged_at, $this->logged_at + 3600 * 24,]);
        }
        if ($this->region !== null) {
            $query->andFilterWhere(['like', RegionI18n::tableName() . '.name', $this->region,]);
        }
        if ($this->fullname !== null) {
            $words = preg_split('/[\s,.]+/', $this->fullname, null, PREG_SPLIT_NO_EMPTY);
            $query->andFilterWhere(['or',
                ['or like', 'firstname', $words,],
                ['or like', 'middlename', $words,],
                ['or like', 'lastname', $words,],
            ]);
        }
        $query->andFilterWhere(['like', 'username', $this->username,])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key,])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash,])
            ->andFilterWhere(['like', 'email', $this->email,])
            ->andFilterWhere(['like', UserProfile::tableName() . '.phone', $this->phone,])
            ->andFilterWhere(['like', CityI18n::tableName() . '.name', $this->city,]);
        if ($this->user_customer_group !== null) {
            if ((int)$this->user_customer_group === $defaultGroup->id) {
                $query->andWhere([
                    'or',
                    [UserProfile::tableName() . '.catalog_user_customer_group_id' => $this->user_customer_group,],
                    [UserProfile::tableName() . '.catalog_user_customer_group_id' => null,],
                ]);
            } else {
                $query->andFilterWhere([
                    UserProfile::tableName() . '.catalog_user_customer_group_id' => $this->user_customer_group,
                ]);
            }
        }
        $query->distinct = false;
        $count = $query->count('DISTINCT ' . static::tableName() . '.id');
        $query->distinct = true;
        $dataProvider->totalCount = $count;
        return $dataProvider;
    }
}

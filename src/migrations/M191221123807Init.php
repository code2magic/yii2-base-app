<?php

namespace code2magic\baseApp\migrations;

use code2magic\baseApp\models\User;
use yii\db\Migration;

/**
 * Class M191220123807Init
 */
class M191221123807Init extends Migration
{
    /**
     * @var null|string
     */
    protected $tableOptions;

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // log
        $this->createTable('{{%system_log}}', [
            'id' => $this->bigPrimaryKey(),
            'level' => $this->integer(),
            'category' => $this->string(),
            'log_time' => $this->double(),
            'prefix' => $this->text(),
            'message' => $this->text(),
        ], $this->tableOptions);
        $this->createIndex('idx_log_level', '{{%system_log}}', 'level');
        $this->createIndex('idx_log_category', '{{%system_log}}', 'category');
        // file_storage_item
        $this->createTable('{{%file_storage_item}}', [
            'id' => $this->primaryKey(),
            'component' => $this->string()->notNull(),
            'base_url' => $this->string(1024)->notNull(),
            'path' => $this->string(1024)->notNull(),
            'type' => $this->string(),
            'size' => $this->integer(),
            'name' => $this->string(),
            'upload_ip' => $this->string(15),
            'created_at' => $this->integer()->notNull()
        ]);
        // timeline_event
        $this->createTable('{{%timeline_event}}', [
            'id' => $this->primaryKey(),
            'application' => $this->string(64)->notNull(),
            'category' => $this->string(64)->notNull(),
            'event' => $this->string(64)->notNull(),
            'data' => $this->text(),
            'created_at' => $this->integer()->notNull()
        ]);
        $this->createIndex('idx_created_at', '{{%timeline_event}}', 'created_at');
        // user
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(32),
            'auth_key' => $this->string(32)->notNull(),
            'access_token' => $this->string(40)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'oauth_client' => $this->string(),
            'oauth_client_user_id' => $this->string(),
            'email' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(User::STATUS_ACTIVE),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'logged_at' => $this->integer()
        ]);
        // user_profile
        $this->createTable('{{%user_profile}}', [
            'user_id' => $this->primaryKey(),
            'firstname' => $this->string(),
            'middlename' => $this->string(),
            'lastname' => $this->string(),
            'adress_region' => $this->string()->notNull()->defaultValue(''),
            'adress_city' => $this->string()->notNull()->defaultValue(''),
            'adress_street' => $this->string()->notNull()->defaultValue(''),
            'adress_building' => $this->string()->notNull()->defaultValue(''),
            'adress_apt' => $this->string()->notNull()->defaultValue(''),
            'avatar_path' => $this->string(),
            'avatar_base_url' => $this->string(),
            'locale' => $this->string(32)->notNull(),
            'gender' => $this->smallInteger(1),
            'phone' => $this->string(50)->notNull()->defaultValue(''),
            'cart' => $this->text(),
            'is_subscribed' => $this->smallInteger(1)->notNull()->defaultValue(1)
        ]);
        $this->addForeignKey(
            'fk-user_profile-2-user',
            '{{%user_profile}}', 'user_id',
            '{{%user}}', 'id',
            'CASCADE', 'CASCADE'
        );
        // user_token
        $this->createTable('{{%user_token}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'type' => $this->string()->notNull(),
            'token' => $this->string(40)->notNull(),
            'expire_at' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
        $this->addForeignKey(
            'fk-user_token-2-user',
            '{{%user_token}}', 'user_id',
            '{{%user}}', 'id',
            'CASCADE', 'CASCADE'
        );
        // semantic_url
        $this->createTable('{{%semantic_url}}', [
            'route' => $this->string(50)->notNull(),
            'id' => $this->integer()->notNull(),
            'url' => $this->string()->notNull(),
        ]);
        $this->addPrimaryKey('pk-semantic_url', '{{%semantic_url}}', ['route', 'id',]);
        $this->createIndex('idx-semantic_url-url', '{{%semantic_url}}', 'url');
        // seed data
        $this->seedData();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // semantic_url
        $this->dropIndex('idx-semantic_url-url', '{{%semantic_url}}');
        $this->dropPrimaryKey('pk-semantic_url', '{{%semantic_url}}');
        $this->dropTable('{{%semantic_url}}');
        // user_token
        $this->dropForeignKey('fk-user_token-2-user', '{{%user_token}}');
        $this->dropTable('{{%user_token}}');
        // user_profile
        $this->dropForeignKey('fk-user_profile-2-user', '{{%user_profile}}');
        $this->dropTable('{{%user_profile}}');
        // user
        $this->dropTable('{{%user}}');
        // timeline_event
        $this->dropTable('{{%timeline_event}}');
        // file_storage_item
        $this->dropTable('{{%file_storage_item}}');
        // log
        $this->dropTable('{{%system_log}}');
    }

    /**
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    private function seedData()
    {
        $language_code_list = $this->db->createCommand('SELECT code FROM {{%language}}')->queryAll(\PDO::FETCH_COLUMN);
        // user
        $this->batchInsert(
            '{{%user}}',
            ['id', 'username', 'email', 'password_hash', 'auth_key', 'access_token', 'status', 'created_at', 'updated_at',],
            [
                [
                    1,
                    'webmaster',
                    'webmaster@example.com',
                    \Yii::$app->getSecurity()->generatePasswordHash('webmaster'),
                    \Yii::$app->getSecurity()->generateRandomString(),
                    \Yii::$app->getSecurity()->generateRandomString(40),
                    User::STATUS_ACTIVE,
                    time(),
                    time(),
                ],
                [
                    2,
                    'manager',
                    'manager@example.com',
                    \Yii::$app->getSecurity()->generatePasswordHash('manager'),
                    \Yii::$app->getSecurity()->generateRandomString(),
                    \Yii::$app->getSecurity()->generateRandomString(40),
                    User::STATUS_ACTIVE,
                    time(),
                    time(),
                ],
                [
                    3,
                    'user',
                    'user@example.com',
                    \Yii::$app->getSecurity()->generatePasswordHash('user'),
                    \Yii::$app->getSecurity()->generateRandomString(),
                    \Yii::$app->getSecurity()->generateRandomString(40),
                    User::STATUS_ACTIVE,
                    time(),
                    time(),
                ],
                [
                    4,
                    'api',
                    'api@example.com',
                    \Yii::$app->getSecurity()->generatePasswordHash('api'),
                    'Kmc2YEWR0sWR6vaqIFm_5uDZNwnQcHe7',
                    'Kmc2YEWR0sWR6vaqIFm_5uDZNwnQcHe7',
                    User::STATUS_ACTIVE,
                    time(),
                    time(),
                ],
            ]
        );
        // user_profile
        $this->batchInsert(
            '{{%user_profile}}',
            ['user_id', 'locale', 'firstname', 'lastname',],
            [
                [
                    1,
                    \Yii::$app->sourceLanguage,
                    'John',
                    'Doe',
                ],
                [
                    2,
                    \Yii::$app->sourceLanguage,
                    '',
                    ''
                ],
                [
                    3,
                    \Yii::$app->sourceLanguage,
                    '',
                    ''
                ],
                [
                    4,
                    \Yii::$app->sourceLanguage,
                    'API',
                    'Demo'
                ],
            ]
        );
        // timeline_event
        $this->batchInsert(
            '{{%timeline_event}}',
            ['application', 'category', 'event', 'data', 'created_at',],
            [
                ['frontend', 'user', 'signup', json_encode(['public_identity' => 'webmaster', 'user_id' => 1, 'created_at' => time(),]), time(),],
                ['frontend', 'user', 'signup', json_encode(['public_identity' => 'manager', 'user_id' => 2, 'created_at' => time(),]), time(),],
                ['frontend', 'user', 'signup', json_encode(['public_identity' => 'user', 'user_id' => 3, 'created_at' => time(),]), time(),],
            ]
        );
        // key_storage_item
        $this->batchInsert(
            '{{%key_storage_item}}',
            ['key', 'value', 'comment',],
            [
                ['backend.theme-skin', 'skin-blue', 'skin-blue, skin-black, skin-purple, skin-green, skin-red, skin-yellow',],
                ['backend.layout-fixed', 0, '',],
                ['backend.layout-boxed', 0, '',],
                ['backend.layout-collapsed-sidebar', 0, '',],
                ['frontend.maintenance', 'disabled', 'Set it to "enabled" to turn on maintenance mode',],
            ]
        );
        // RBAC
        /**
         * @var \yii\rbac\DbManager $authManager
         */
        $authManager = \Yii::$app->get('authManager');
        //// user
        $user = $authManager->createRole(User::ROLE_USER);
        $authManager->add($user);
        //// manager
        $manager = $authManager->createRole(User::ROLE_MANAGER);
        $authManager->add($manager);
        $authManager->addChild($manager, $user);
        //// admin
        $admin = $authManager->createRole(User::ROLE_ADMINISTRATOR);
        $authManager->add($admin);
        $authManager->addChild($admin, $manager);
        $authManager->addChild($admin, $user);
        //// assign user to roles
        $authManager->assign($admin, 1);
        $authManager->assign($manager, 2);
        $authManager->assign($user, 3);
        //// permissions
        $loginToBackend = $authManager->createPermission('loginToBackend');
        $authManager->add($loginToBackend);
        $authManager->addChild($manager, $loginToBackend);
        $authManager->addChild($admin, $loginToBackend);
    }
}

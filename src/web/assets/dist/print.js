"use strict";
/**
 *
 * @param html
 * @param window_params
 */
function printHtml(html, window_params){
    window_params= window_params ? window_params : 'height=400,width=600';
    var print_window = window.open('', 'PRINT', window_params);

    print_window.document.write(html);

    print_window.document.close(); // necessary for IE >= 10
    print_window.focus(); // necessary for IE >= 10

    print_window.print();
    print_window.close();
}

<?php

namespace code2magic\baseApp\web\assets;

/**
 * Class PrintAsset
 * @package common\assets
 */
class PrintAsset
{
    /**
     * @var string
     */
    public $sourcePath = __DIR__ . '/dist';

    /**
     * @var array
     */
    public $js = [
        'print.js'
    ];
}

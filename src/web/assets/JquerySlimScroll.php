<?php

namespace code2magic\baseApp\web\assets;

use yii\web\AssetBundle;

/**
 * Class JquerySlimScroll
 * @package common\assets
 */
class JquerySlimScroll extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $sourcePath = '@npm/jquery-slimscroll';

    /**
     * @inheritDoc
     */
    public $js = [
        'jquery.slimscroll.min.js',
    ];

    /**
     * @inheritDoc
     */
    public $depends = [
        \yii\web\JqueryAsset::class,
    ];
}

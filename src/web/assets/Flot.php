<?php

namespace code2magic\baseApp\web\assets;

use yii\web\AssetBundle;

/**
 * Class Flot
 * @package common\assets
 */
class Flot extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $sourcePath = '@npm/flot';

    /**
     * @inheritDoc
     */
    public $js = [
        'jquery.flot.js',
    ];

    /**
     * @inheritDoc
     */
    public $depends = [
        \yii\web\JqueryAsset::class,
    ];
}

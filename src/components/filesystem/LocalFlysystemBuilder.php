<?php

namespace code2magic\baseApp\components\filesystem;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use trntv\filekit\filesystem\FilesystemBuilderInterface;

/**
 * Class LocalFlysystemProvider
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class LocalFlysystemBuilder implements FilesystemBuilderInterface
{
    /**
     * @var 
     */
    public $path;

    /**
     * @return Filesystem|mixed
     */
    public function build()
    {
        $adapter = new Local(\Yii::getAlias($this->path));
        return new Filesystem($adapter);
    }
}

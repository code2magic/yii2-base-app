<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 * @var string $maintenanceText
 * @var int|string $retryAfter
 */
?>
<div id="maintenance-content" style="margin-top: 10%">
    <p class="well">
        <?= $maintenanceText ?>
    </p>
</div>

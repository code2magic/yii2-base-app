<?php

namespace code2magic\baseApp\components\maintenance;

use yii\web\AssetBundle;

/**
 * Class MaintenanceAsset
 * @package code2magic\baseApp\components\maintenance
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class MaintenanceAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = __DIR__ . '/assets';

    /**
     * @var array
     */
    public $css = [
        'css/maintenance.css',
    ];

    /**
     * @var array
     */
    public $depends = [
        \yii\web\YiiAsset::class,
        \yii\bootstrap\BootstrapAsset::class,
    ];
}

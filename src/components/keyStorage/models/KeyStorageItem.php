<?php

namespace code2magic\baseApp\components\keyStorage\models;

use code2magic\core\behaviors\CacheInvalidateBehavior;
use Yii;
use yii\behaviors\AttributesBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "key_storage_item".
 *
 * @property integer $key
 * @property integer $value
 * @property string $comment
 * @property int $updated_at [int(11)]
 * @property int $created_at [int(11)]
 */
class KeyStorageItem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%key_storage_item}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'skipUpdateOnClean' => false,
            ],
            'json' => [
                'class' => AttributesBehavior::class,
                'attributes' => ['value' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => $fn = function ($event, $attribute) {
                        if (\is_array($this->$attribute)) {
                            $this->is_json = 1;
                            return json_encode($this->$attribute);

                        } else {
                            return $this->$attribute;
                        }
                    },
                    ActiveRecord::EVENT_BEFORE_UPDATE => $fn,
                    ActiveRecord::EVENT_AFTER_FIND => function ($event, $attribute) {
                        return $this->is_json ? json_decode($this->value, true) : $this->value;
                    },
                ],],
                'skipUpdateOnClean' => false,
            ],
            'cacheInvalidate' => [
                'class' => CacheInvalidateBehavior::class,
                'tags' => [
                    function ($model) {
                        return
                            self::class .
                            $model->key;
                    },
                    'key-storage',
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'value'], 'required'],
            [['key'], 'string', 'max' => 128],
            [['value', 'comment'], 'safe'],
            [['key'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'key' => Yii::t('model_labels', 'Key'),
            'value' => Yii::t('model_labels', 'Value'),
            'comment' => Yii::t('model_labels', 'Comment'),
        ];
    }
}

<?php

namespace code2magic\baseApp\components\keyStorage;

use kartik\tabs\TabsX;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class FormWidget extends Widget
{
    /**
     * @var \code2magic\baseApp\components\keyStorage\FormModel
     */
    public $model;
    /**
     * @var string
     */
    public $formClass = \yii\widgets\ActiveForm::class;
    /**
     * @var array
     */
    public $formOptions;
    /**
     * @var string
     */
    public $submitText;
    /**
     * @var array
     */
    public $submitOptions;

    /**
     * @throws InvalidConfigException
     */
    public function run()
    {
        $model = $this->model;
        $tabs = [];
        $form = \call_user_func([$this->formClass, 'begin'], $this->formOptions);
        foreach ($model->keys as $key => $config) {
            $type = ArrayHelper::getValue($config, 'type', FormModel::TYPE_TEXTINPUT);
            $options = ArrayHelper::getValue($config, 'options', []);
            $field = $form->field($model, $key);
            $items = ArrayHelper::getValue($config, 'items', []);
            $tab = ArrayHelper::getValue($config, 'tab', \Yii::t('backend', 'main'));
            switch ($type) {
                case FormModel::TYPE_TEXTINPUT:
                    $input = $field->textInput($options);
                    break;
                case FormModel::TYPE_DROPDOWN:
                    $input = $field->dropDownList($items, $options);
                    break;
                case FormModel::TYPE_CHECKBOX:
                    $input = $field->checkbox($options);
                    break;
                case FormModel::TYPE_CHECKBOXLIST:
                    $input = $field->checkboxList($items, $options);
                    break;
                case FormModel::TYPE_RADIOLIST:
                    $input = $field->radioList($items, $options);
                    break;
                case FormModel::TYPE_TEXTAREA:
                    $input = $field->textarea($options);
                    break;
                case FormModel::TYPE_WIDGET:
                    $widget = ArrayHelper::getValue($config, 'widget');
                    if ($widget === null) {
                        throw new InvalidConfigException('Widget class must be set');
                    }
                    $input = $field->widget($widget, $options);
                    break;
                default:
                    $input = $field->input($type, $options);

            }
            if (!array_key_exists($tab, $tabs)) {
                $tabs[$tab] = [];
            }
            $tabs[$tab][] = $input;
        }
        $parts = [];
        foreach ($tabs as $n => $tab) {
            $parts[] = [
                'label' => \Yii::t('backend', $n),
                'content' => $this->render('_tab', [
                    'inputs' => $tab
                ])
            ];
        }
        echo TabsX::widget([
            'items' => $parts
        ]);
        echo Html::submitButton($this->submitText, $this->submitOptions);
        \call_user_func([$this->formClass, 'end']);
    }
}

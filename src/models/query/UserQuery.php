<?php

namespace code2magic\baseApp\models\query;

use code2magic\baseApp\models\User;
use yii\db\ActiveQuery;

/**
 * Class UserQuery
 * @package code2magic\baseApp\models\query
 * @author Eugene Terentev <eugene@terentev.net>
 */
class UserQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function notDeleted()
    {
        $this->andWhere(['<', 'status', User::STATUS_ACTIVE]);
        return $this;
    }

    /**
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['>=', 'status', User::STATUS_ACTIVE]);
        return $this;
    }

    /**
     * @return $this
     */
    public function today(){
        $this->andWhere(['>=', 'created_at', strtotime('-1 day')]);
        return $this;
    }
}

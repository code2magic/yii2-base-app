<?php

namespace code2magic\baseApp\models;

use code2magic\baseApp\models\query\TimelineEventQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "timeline_event".
 *
 * @property integer $id
 * @property string  $application
 * @property string  $category
 * @property string  $event
 * @property string  $data
 * @property string  $created_at
 */
class TimelineEvent extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%timeline_event}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null
            ]
        ];
    }

    /**
     * @return TimelineEventQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new TimelineEventQuery(static::class);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['application', 'category', 'event'], 'required'],
            [['data'], 'safe'],
            [['application', 'category', 'event'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->data = @json_decode($this->data, true);
        parent::afterFind();
    }

    /**
     * @param      $category
     * @param      $event
     * @param null $data
     *
     * @return bool
     */
    public static function log($category, $event, $data = null)
    {
        $model = new self();
        $model->application = Yii::$app->id;
        $model->category = $category;
        $model->event = $event;
        $model->data = json_encode($data, JSON_UNESCAPED_UNICODE);
        return $model->save(false);
    }

    /**
     * @return string
     */
    public function getFullEventName(): string
    {
        return sprintf('%s.%s', $this->category, $this->event);
    }
}

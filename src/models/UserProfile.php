<?php

namespace code2magic\baseApp\models;

use api\modules\v1\models\OrderShipment;
use borales\extensions\phoneInput\PhoneInputBehavior;
use borales\extensions\phoneInput\PhoneInputValidator;
use code2magic\catalog\models\i18n\UserCustomerGroupI18n;
use code2magic\catalog\models\UserCustomerGroup;
use code2magic\geo\models\City;
use code2magic\i18n\models\Language;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user_profile".
 *
 * @property int $user_id [int(11)]
 * @property string $id_1c [varchar(100)]
 * @property string $firstname [varchar(255)]
 * @property string $middlename [varchar(255)]
 * @property string $lastname [varchar(255)]
 * @property string $address_city [varchar(255)]
 * @property int $geo_city_id [int(11)]
 * @property string $address_region [varchar(255)]
 * @property string $address_street [varchar(255)]
 * @property string $address_building [varchar(255)]
 * @property string $address_apt [varchar(255)]
 * @property string $avatar_path [varchar(255)]
 * @property string $avatar_base_url [varchar(255)]
 * @property string $locale [varchar(32)]
 * @property int $gender [smallint(1)]
 * @property string $phone [varchar(50)]
 * @property string $cart
 * @property int $is_subscribed [smallint(1)]
 * @property int $catalog_user_customer_group_id [int(11)]
 * @property string $birthday [date]
 * @property string $comment
 * @property string $data [json]
 *
 * @property null|string $fullName
 * @property null|string $fullAddress
 * @property null|UserCustomerGroup $userCustomerGroup
 * @property null|User $user
 * @property null|City $city
 */
class UserProfile extends ActiveRecord
{
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    /**
     * @var
     */
    public $picture;

    //<editor-fold desc="ActiveRecord">

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_profile}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'picture' => [
                'class' => UploadBehavior::class,
                'attribute' => 'picture',
                'pathAttribute' => 'avatar_path',
                'baseUrlAttribute' => 'avatar_base_url',
            ],
            'phoneInput' => PhoneInputBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id',], 'required',],
            [['user_id', 'gender',], 'integer',],
            [['gender',], 'in', 'range' => [null, self::GENDER_FEMALE, self::GENDER_MALE,],],
            [
                [
                    'firstname',
                    'lastname',
                    'middlename',
                    'address_region',
                    'address_city',
                    'address_apt',
                    'address_street',
                    'address_building',
                ],
                'string',
                'max' => 255,
            ],
            [['birthday',], 'date',],
            [['locale',], 'default', 'value' => Yii::$app->language,],
            [['locale',], 'exist', 'targetClass' => Language::class, 'targetAttribute' => ['locale' => 'code',],],
            [['picture',], 'safe',],
            [['is_subscribed',], 'boolean',],
            [['is_subscribed',], 'default', 'value' => 1,],
            [['phone',], 'string', 'max' => 50,],
            [['phone',], PhoneInputValidator::class],
            [['comment',], 'string',],
            [['catalog_user_customer_group_id',], 'exist', 'targetRelation' => 'userCustomerGroup',],
            [['geo_city_id',], 'exist', 'targetRelation' => 'city',],
            [['address_region',], 'default', 'value' => function ($model, $attribute) {
                return ArrayHelper::getValue($model, ['city', 'region', 'name',], '');
            }],
            [['address_city',], 'default', 'value' => function ($model, $attribute) {
                return ArrayHelper::getValue($model, ['city', 'name',], '');
            }],
            [['shipmentId'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('model_labels', 'User ID'),
            'firstname' => Yii::t('model_labels', 'Firstname'),
            'middlename' => Yii::t('model_labels', 'Middlename'),
            'lastname' => Yii::t('model_labels', 'Lastname'),
            'locale' => Yii::t('model_labels', 'Locale'),
            'picture' => Yii::t('model_labels', 'Picture'),
            'gender' => Yii::t('model_labels', 'Gender'),
            'phone' => Yii::t('model_labels', 'Phone'),
            'address_region' => Yii::t('model_labels', 'Address Region'),
            'address_building' => Yii::t('model_labels', 'Address Building'),
            'address_city' => Yii::t('model_labels', 'Address City'),
            'geo_city_id' => Yii::t('model_labels', 'City'),
            'address_street' => Yii::t('model_labels', 'Address Street'),
            'address_apt' => Yii::t('model_labels', 'Address Apt'),
            'is_subscribed' => Yii::t('model_labels', 'Get subscribe'),
            'birthday' => Yii::t('model_labels', 'Birthday'),
            'comment' => Yii::t('model_labels', 'Comment'),
        ];
    }

    //</editor-fold>
    //<editor-fold desc="Relations">

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id',]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCustomerGroup()
    {
        if ($this->catalog_user_customer_group_id) {
            return $this->hasOne(UserCustomerGroup::class, ['id' => 'catalog_user_customer_group_id',]);
        }
        $query = UserCustomerGroup::find()->default();
        $query->link = ['id' => 'catalog_user_customer_group_id',];
        $query->multiple = false;
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCustomerGroupI18n($lang = null)
    {
        $query = $this->hasOne(UserCustomerGroupI18n::class, ['catalog_user_customer_group_id' => 'id',])
            ->via('userCustomerGroup')
            ->andWhere([UserCustomerGroupI18n::tableName() . '.language_code' => $lang ?? Yii::$app->language,]);
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'geo_city_id',]);
    }

    //</editor-fold>

    /**
     * @param null $default
     *
     * @return bool|null|string
     */
    public function getAvatar($default = null)
    {
        return $this->avatar_path
            ? Yii::getAlias($this->avatar_base_url . '/' . $this->avatar_path)
            : $default;
    }

    /**
     * @return null|string
     */
    public function getFullName()
    {
        return implode(' ', [$this->firstname, $this->middlename, $this->lastname,]);
    }

    /**
     * @return null|string
     */
    public function getFullAddress()
    {
        return implode(' ', [$this->address_region, $this->address_city, $this->address_street, $this->address_building, $this->address_apt,]);
    }

    public function getShipmentData()
    {
        return ArrayHelper::getValue($this->data, 'shipmentData');
    }

    public function setShipmentData($value)
    {
        $data = $this->data;
        ArrayHelper::setValue($data, 'shipmentData', $value);
        $this->data = $data;
    }

    public function setShipmentId($id)
    {
        $data = $this->data;
        ArrayHelper::setValue($data, 'shipmentId', $id);
        $this->data = $data;
    }

    public function getShipment_api_type()
    {
        $shipment = OrderShipment::find()->andWhere([OrderShipment::tableName() . '.id' => $this->getShipmentId()])->one();
        return $shipment ? $shipment->api_type : null;
    }

    public function getShipmentId()
    {
        return ArrayHelper::getValue($this->data, 'shipmentId');
    }
}

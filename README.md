Yii2 base app
==========

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run
```
php composer.phar require code2magic/yii2-base-app --prefer-dist
```
or add
```
"code2magic/yii2-base-appt": "*"
```

to the `require` section of your `composer.json` file.

## Configuration

This extension is supposed to be used with [composer-config-plugin].

Else look files for cofiguration example:

* [src/backend/config/main.php]
* [src/common/config/base.php]
* [src/common/config/web.php]
* [src/frontend/config/main.php]


## Usage


[composer-config-plugin]:           https://github.com/hiqdev/composer-config-plugin
[src/backend/config/main.php]:      src/backend/config/main.php
[src/common/config/base.php]:       src/common/config/base.php
[src/common/config/web.php]:        src/common/config/web.php
[src/frontend/config/main.php]:     src/frontend/config/main.php
